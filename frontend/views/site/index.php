<?php
$layout = isset($this->params['layout']) ? $this->params['layout'] : '';
$homePath = $layout === '' ? 'site' : $layout;
$user = \common\models\User::getCurrentUser();
?>
<div class="market_holder clearfix">
	<aside class="left_sidebar">
	    <div class="search_block">
	        <form action="">
	            <input type="text" name="search" value="" placeholder="<?= \yii::t('app', 'Поиск по названию'); ?>" />
	            <button class="submit_search"><i class="fa fa-search"></i></button>
	        </form>
	    </div>
	    <div class="search_filters">
	        <div class="market_list">
	            <div class="item" onclick="location.href='/'" style="cursor: pointer">
	                <?php if($layout == ''): ?>
                        <img src="/images/icon_dota_red.png" alt="dota" />
                    <?php else: ?>
	                    <img src="/images/icon_dota_not_active.png" alt="dota" />
                    <?php endif; ?>
	            </div>
	            <div class="item" onclick="location.href='/cs'" style="cursor: pointer">
                    <?php if($layout == 'cs'): ?>
                        <img src="/images/icon_cs_active.png" alt="cs" />
                    <?php else: ?>
                        <img src="/images/icon_cs.png" alt="cs" />
                    <?php endif; ?>
	            </div>
	        </div>
	        <div class="filters_holder">
	            <div class="input_holder price">
	                <label><?= \yii::t('app', 'Стоимость'); ?></label>
	                <div class="slider_inputs">
	                        <span>
	                            <label for="input_from"><?= \yii::t('app', 'от'); ?></label>
	                            <input id="input_from" type="text" />
	                        </span>
	                        <span>
	                            <label for="input_to"><?= \yii::t('app', 'до'); ?></label>
	                            <input id="input_to" type="text" />
	                        </span>
	                </div>
	                <div class="slider_holder">
	                    <div id="price_slider">
	                        <input type="hidden" class="slider-input" value="" />
	                    </div>
	                </div>
	            </div>
	
	            <div class="input_holder">
	                <label><?= \yii::t('app', 'Раритетность'); ?></label>
	                <input type="text" name="" value="" placeholder="<?= \yii::t('app', 'Все'); ?>" />
	            </div>
	            <div class="input_holder">
	                <label><?= \yii::t('app', 'Качество'); ?></label>
	                <input type="text" name="" value="" placeholder="<?= \yii::t('app', 'Все'); ?>" />
	            </div>
	            <div class="input_holder">
	                <label><?= \yii::t('app', 'Тип'); ?></label>
	                <input type="text" name="" value="" placeholder="<?= \yii::t('app', 'Все'); ?>" />
	            </div>
	            <div class="input_holder">
	                <label><?= \yii::t('app', 'Герои'); ?></label>
	                <input type="text" name="" value="" placeholder="<?= \yii::t('app', 'Все'); ?>" />
	            </div>
	        </div>
	
	        <div class="bottom_label">
	                <span class="icon">
	                    <i class="fa fa-steam"></i>
	                </span>
	            <span><?= \yii::t('app', 'дешевле, чем в Steam'); ?></span>
	        </div>
	    </div>
	</aside>
	<div class="content_holder">
		<?php if(\common\models\Settings::find()->one()->stop_trade === 0): ?>
			<div class="sorter_holder">
				<span class="sorter_title"><?= \yii::t('app', 'Сортировка по:'); ?></span>
				<ul class="sorter">
					<li class="active"><span><?= \yii::t('app', 'цене'); ?></span></li>
					<li><span><?= \yii::t('app', 'названию'); ?></span></li>
					<li><span><?= \yii::t('app', 'количеству'); ?></span></li>
				</ul>
			</div>
	
			<div class="buy_block market">
				<div class="buy_holder clearfix">
					<?php foreach($items as $item): ?>
						<div class="item">
						<a href="">
							<div class="image_block">
								<img src="/images/data/buy_image.png"/>
								<div class="price">
									<span>42.48 P</span>
										<span class="icon">
											<img src="/images/icon_dota.png" alt="dota" />
										</span>
								</div>
							</div>
							<div class="title">
								<span>Treasure Upgrade Infuser - Winter 2016</span>
							</div>
						</a>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
	
	    <div class="pager_holder">
	        <span class="arrow prev"><?= \yii::t('app', 'Предыдущая страница'); ?></span>
	        <div class="pager">
	            <div class="input_holder">
	                <input type="tel" pattern="(\+?\d[- .]*){7,13}" value="1" />
	            </div>
	            <span><?= \yii::t('app', 'из'); ?> 492</span>
	        </div>
	        <a class="arrow next"><?= \yii::t('app', 'Следующая страница'); ?></a>
	    </div>
		<?php else: ?>
			<p> Торговля временно остановлена!</p>
		<?php endif; ?>
	</div>
</div>
<?= $this->render('/profile/parts/buy_history', []) ?>
<!--<div class="buy_block buy_history">-->
<!--    <div class="title_block">-->
<!--        <span class="buy_icon"><i class="fa fa-shopping-cart"></i></span>-->
<!--        <h3>--><?//= \yii::t('app', 'История покупок на нашей площадке'); ?><!--</h3>-->
<!--        <a class="show_all" href="#">--><?//= \yii::t('app', 'Всего ') . 9096901 . \yii::t('app', ' покупок'); ?><!--</a>-->
<!--    </div>-->
<!--    <div class="buy_holder clearfix">-->
<!--        --><?php //foreach($all as $item): ?>
<!--            <div class="item">-->
<!--                <a href="">-->
<!--                    <div class="image_block">-->
<!--                        <img src="/images/data/buy_image.png"/>-->
<!--                        <div class="price">-->
<!--                            <span>42.48 P</span>-->
<!--                                    <span class="icon">-->
<!--                                        <img src="/images/icon_dota.png" alt="dota" />-->
<!--                                    </span>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="title">-->
<!--                        <span>Treasurae Upgrade Infuser - Winter 2016</span>-->
<!--                    </div>-->
<!--                </a>-->
<!--            </div>-->
<!--        --><?php //endforeach; ?>
<!--    </div>-->
<!--</div>-->