<h1><?= \yii::t('app', 'Накопительные скидки'); ?></h1>
<div class="page_desc">
    <p>
        <?= \yii::t('app', 'Всем пользователям нашего маркета мы предлагаем накопительные скидки. Скидка начинает накапливаться, как
        только вы начинаете пользоваться сервисом, и растёт с каждой покупкой и продажей.'); ?>
        </p>
    <p><?= \yii::t('app', 'Размер скидки при покупке зависит от общей суммы, потраченной в магазине, а размер комиссии при продаже
        — от суммарной стоимости ваших вещей.'); ?></p>
    <p><?= \yii::t('app', 'Пример зависимости скидки от оборота:'); ?></p>
</div>
<div class="discount_table">
    <table class="table_discounts">
        <thead>
        <tr>
            <th>Потрачено в магазине</th>
            <th>Скидка на покупки</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($discounts as $discount): ?>
            <tr>
                <td><?= $discount->amount; ?>&nbsp;руб</td>
                <td><?= $discount->discount; ?>%</td>
            </tr>
        <?php endforeach; ?>
        <?php if (empty($discounts)): ?>
            <tr>
                <td colspan="2" style="text-align: left">
                    <?= \yii::t('app', 'Not available yet'); ?>
                </td>
            </tr>
        <?php endif; ?>
        <?php if(!Yii::$app->user->isGuest) { ?>
            <td><?= $amount ?  $amount : 0 ?>&nbsp;руб</td>
            <td><?= $real_discount ? \yii::t('app', 'Текущая скидка | '). $real_discount : \yii::t('app', 'Текущая скидка | 0'); ?>%</td>
        <?php } ?>
        </tbody>
    </table>

    <table class="table_comissions">
        <thead>
        <tr>
            <th>Получено с продаж</th>
            <th>Комиссия при продаже</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($comissions as $comission): ?>
            <tr>
                <td><?= $comission->amount; ?>&nbsp;руб</td>
                <td><?= $comission->overcharge; ?>%</td>
            </tr>
        <?php endforeach; ?>
        <?php if (empty($comissions)): ?>
            <tr>
                <td colspan="2" style="text-align: left">
                    <?= \yii::t('app', 'Not available yet'); ?>
                </td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>
<div style="clear: both"></div>
<?= $this->render('/profile/parts/buy_history', []) ?>
