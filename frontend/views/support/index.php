<?php $layout = isset($this->params['layout']) ? $this->params['layout'] : ''; ?>
<div class="content_holder account">
    <h1><?= \yii::t('app', 'Техподдержка'); ?></h1>
    <div class="support_form">
        <div class="title">
            <h5><?= \yii::t('app', 'Создать новый тикет'); ?></h5>
        </div>

        <?php if (!Yii::$app->user->isGuest) : ?>
            <?php $form = \yii\widgets\ActiveForm::begin([
                'id' => 'new_ticket',
                'options' => ['class' => 'form-horizontal'],
            ]) ?>
                <div class="form">
                    <div class="input_holder">
                        <?= $form->field($tickets, 'theme_id')->dropDownList($themes)->label('1. Укажите тему тикета', ['style' => 'text-align:left']); ?>
                        <?= $form->field($tickets, 'other_theme')->textInput(['placeholder' => 'Введите тему', 'style' => 'display:none'])->label(false) ?>
                    </div>
                    <div class="input_holder">
                      <?= $form->field($tickets, 'text')->textarea(['id' => 'problem'])->label('2. Подробно опишите проблему (тикеты без подробного описания будут закрываться)', ['style' => 'text-align:left']) ?>
                    </div>
                    <div class="input_holder checkbox_holder">
                        <input id="tickets-checkbox" type="checkbox" name="read_faq" />
                        <label for="tickets-checkbox">3. Я прочитал <a href="#faqs">часто задаваемые вопросы</a></label>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-1 col-lg-11 input_holder">
                            <?= \yii\helpers\Html::submitButton('Создать тикет', ['class' => 'btn button btn-primary', 'id' => 'create_ticket_btn', 'disabled' => 'disabled']) ?>
                        </div>
                    </div>
                </div>
            <?php \yii\widgets\ActiveForm::end(); ?>
        <?php else: ?>
            <p><?= \common\models\Blocks::getBlock('tp_not_login') ?></p>
            <?php $login = '/site/login?service=steam'; ?>
            <?php if($layout == 'cs'){
                $login = '/cs/login?service=steam';
            } ?>
            <a class="steam_holder" href="<?= $login; ?>" data-eauth-service="steam">
                <div class="steam_block clearfix">
                    <div class="logo">
                        <img src="/images/steam_logo.png" alt="Steam" />
                    </div>
                    <div class="text">
                        <p ><?= \yii::t('app', 'Авторизоваться через '); ?><strong>Steam</strong></p>
                    </div>
                </div>
            </a>
        <?php endif; ?>
    </div>
    <div id="faqs" class="faq_block">
        <h3>Часто задаваемые вопросы</h3>
        <div class="faq_list">
            <?php foreach($faqs as $faq): ?>

            <div class="item">
                <div class="question">
                    <i class="fa fa-question-circle"></i>
                    <span><?= \common\models\Faqs::getQuestion($faq->id)?></span>
                </div>
                <div class="answer">
                    <p><?= \common\models\Faqs::getAnswer($faq->id)?></p>
                </div>
            </div>
                <?php endforeach; ?>
        </div>
    </div>
</div>