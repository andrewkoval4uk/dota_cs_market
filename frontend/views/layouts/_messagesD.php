<div class="message_block" id="message_block">
    <?php
        if(!isset($layout) || $layout === '') {
            $model = \common\models\Dcchats::find()->limit(3000)->all();
        } else {
            $model = \common\models\Cschats::find()->limit(3000)->all();
        }
    ?>
    <?php foreach($model as $m): ?>
        <div class="message">
            <a href="#" class="user"><?= $m->nickname; ?></a>
            <span class="<?= $m->status ? 'spam_message ' : '' ?> text"><?= $m->status ? 'отмечено как спам' : $m->message; ?></span>
            <span onclick="setReportStatusToMessage(<?=$m->id ?>)" id="chat-report-button-<?=$m->id ?>" data-url="<?= \yii\helpers\Url::toRoute('site/setreport'); ?>" class="report">Report</span>
        </div>
    <?php endforeach; ?>
</div>