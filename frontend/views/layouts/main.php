﻿<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use \yii\helpers\Url;
use \common\models\Settings;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <link href='https://fonts.googleapis.com/css?family=Play:400,700&subset=cyrillic-ext,cyrillic,latin-ext,latin' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <?= Html::csrfMetaTags() ?>
    <title>SteamMarket</title>
    <?php $this->head() ?>
    <?php $layout = isset($this->params['layout']) ? $this->params['layout'] : ''; ?>
    <?php $homePath = $layout === '' ? 'site' : $layout; ?>
    <?php $user = \common\models\User::getCurrentUser(); ?>
</head>
<body <?= $layout !== '' ? 'id="' . $layout . '"' : '' ?>>
<?php $this->beginBody() ?>
<?php if(Settings::find()->one()->stop_trade === 1): ?>
    <?php if(!isset(Yii::$app->request->cookies['stop_trade'])):?>
        <?php Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => 'stop_trade',
            'value' => true
        ])); ?>
        <div id="popup-stop-trade" class="modal-stop-trade">
            <div class="modal-stop-trade">
                <span class="close-modal-stop-trade">x</span>
                <h4><?= \yii::t('app', 'Внимание! Торговля на сайте временно остановлена! Ожидайте обновлений'); ?></h4>
                <div class="form-group">
                    <div class="col-lg-offset-1 col-lg-11 input_holder">
                        <button id="close_notification_btn" class="btn button btn-primary"><?= \yii::t('app', 'Закрыть'); ?></button>
                    </div>
                </div>
            </div>
        </div>
    <div id="overlay"></div>
    <?php endif; ?>
<?php elseif(isset(Yii::$app->request->cookies['stop_trade'])): ?>
    <?php Yii::$app->response->cookies->remove('stop_trade'); ?>
<?php endif; ?>
<section id="wrapper">
    <div id="main_bg"></div>
    <header id="header">
        <div class="container clearfix">
            <div id="logo">
                <a href="/<?= $layout ?>">
                    <img src="/images/logo.png" alt="<?= $layout !== '' ? $layout : 'Dota2' ?>" />
                </a>
            </div>

            <nav id="main_menu" class="clearfix">
                <ul>
                    <?php if($layout === ''): ?>
                        <li class="<?= Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index' ? 'active' : ''; ?>">
                            <a href="<?= Url::toRoute('/'); ?>" title="<?= \yii::t('app', 'Предложения'); ?>">
                                <?= \yii::t('app', 'Предложения'); ?>
                            </a>
                        </li>
                        <li class="<?= Yii::$app->controller->id == 'profile' ? 'active' : ''; ?>">
                            <a href="<?= Url::toRoute('/profile'); ?>" title="<?= \yii::t('app', 'Инвентарь'); ?>">
                                <?= \yii::t('app', 'Инвентарь'); ?>
                            </a>
                        </li>
                        <li class="<?= Yii::$app->controller->id == 'support' ? 'active' : ''; ?>">
                            <a href="<?= Url::toRoute('/support'); ?>" title="<?= \yii::t('app', 'Техподдержка'); ?>">
                                <?= \yii::t('app', 'Техподдержка'); ?>
                            </a></li>
                        <li class="<?= Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'discount' ? 'active' : ''; ?>">
                            <a href="<?= !Yii::$app->user->isGuest ? Url::toRoute(['/site/discount', 'id'=>$user->id]) : Url::toRoute('/site/discount') ; ?>" title="<?= \yii::t('app', 'Скидки'); ?>">
                                <?= \yii::t('app', 'Скидки'); ?>
                            </a>
                        </li>
                        <li class="<?= Yii::$app->controller->id == 'news' ? 'active' : ''; ?>">
                            <a href="<?= Url::toRoute('/news'); ?>" title="<?= \yii::t('app', 'Новости'); ?>">
                                <?= \yii::t('app', 'Новости'); ?>
                            </a>
                        </li>
                        <li class="<?= Yii::$app->controller->id == 'pages' && isset($_GET['view']) == 'about' ? 'active' : ''; ?>">
                            <a href="<?= Url::toRoute(['/pages', 'view'=>'about']); ?>" title="<?= \yii::t('app', 'Информация'); ?>">
                                <?= \yii::t('app', 'Информация'); ?>
                            </a>
                        </li>
                    <?php elseif($this->params['layout'] == 'cs'):?>
                        <li class="<?= Yii::$app->controller->id == 'cs' && Yii::$app->controller->action->id == 'index' ? 'active' : ''; ?>">
                            <a href="<?= Url::toRoute('/cs'); ?>" title="<?= \yii::t('app', 'Предложения'); ?>">
                                <?= \yii::t('app', 'Предложения'); ?>
                            </a>
                        </li>
                        <li class="<?= Yii::$app->controller->id == 'csprofile' ? 'active' : ''; ?>">
                            <a href="<?= Url::toRoute('/csprofile'); ?>" title="<?= \yii::t('app', 'Инвентарь'); ?>">
                                <?= \yii::t('app', 'Инвентарь'); ?>
                            </a>
                        </li>
                        <li class="<?= Yii::$app->controller->id == 'cssupport' ? 'active' : ''; ?>">
                            <a href="<?= Url::toRoute('/cssupport'); ?>" title="<?= \yii::t('app', 'Техподдержка'); ?>">
                                <?= \yii::t('app', 'Техподдержка'); ?>
                            </a></li>
                        <li class="<?= Yii::$app->controller->id == 'cs' && Yii::$app->controller->action->id == 'discount' ? 'active' : ''; ?>">
                            <a href="<?= Url::toRoute('/cs/discount'); ?>" title="<?= \yii::t('app', 'Скидки'); ?>">
                                <?= \yii::t('app', 'Скидки'); ?>
                            </a>
                        </li>
                        <li class="<?= Yii::$app->controller->id == 'csnews' ? 'active' : ''; ?>">
                            <a href="<?= Url::toRoute('/csnews'); ?>" title="<?= \yii::t('app', 'Новости'); ?>">
                                <?= \yii::t('app', 'Новости'); ?>
                            </a>
                        </li>
                        <li class="<?= Yii::$app->controller->id == 'cspages' && $_GET['view'] == 'about' ? 'active' : ''; ?>">
                            <a href="<?= Url::toRoute(['/cspages', 'view'=>'about']); ?>" title="<?= \yii::t('app', 'Информация'); ?>">
                                <?= \yii::t('app', 'Информация'); ?>
                            </a>
                        </li>
                    <?php endif; ?>
                </ul>
            </nav>

            <?php if(Yii::$app->user->isGuest): ?>
            <div id="header_profile">
                <?php
                $login = '/site/login?service=steam';
                if($layout == 'cs'){
                    $login = '/cs/login?service=steam';
                } ?>
                <a class="steam_holder" href="<?= $login; ?>" data-eauth-service="steam">
                    <div class="steam_block clearfix">
                        <div class="logo">
                            <img src="/images/steam_logo.png" alt="Steam" />
                        </div>
                        <div class="text">
                            <p ><?= \yii::t('app', 'Авторизоваться через '); ?><strong>Steam</strong></p>
                        </div>
                    </div>
                </a>
            </div>
            <?php else: ?>
            <div id="header_profile">
                <div class="info">
                    <?php
                    $identity = Yii::$app->getUser()->getIdentity();
                    $id = $identity->profile['name'];
                    $name = \common\models\User::getSteamName($id);
                    ?>
                    <h4 class="name"><?= $name ?></h4>

                    <ul class="desc">
                        <li>
                            <span class="cash"><?= $user->balance ? number_format($user->balance, 0, ',', ' ') : '0'?> P</span>
                        </li>
                        <li>
                            <a class="icon cash_icon" href="<?= Url::toRoute(['profile/cashout', 'id'=>$user->id]); ?>"><i class="fa fa-money"></i></a>
                        </li>
                        <li>
                            <span class="show-notification-icon icon">
                                <i class="fa fa-bell"></i>
                                <span id="notifications_count" class="count"><?= count(\common\models\Notifications::getAllUserNotifications($user->id))?></span>
                            </span>
                            <ul id="notifications_block" style="display: none;">
                                <?= $this->render('_notifications', [
                                    'layout' => $layout,
                                ]); ?>
                            </ul>

                        </li>
                        <li>
                        <?=  Html::a('',
                            ['/' . $homePath . '/logout'],
                            [
                                'class'       => 'fa fa-sign-out',
                                'data-method' => 'post',
                            ]);?>
                        </li>
                    </ul>
                </div>

                <div class="avatar">
                    <a href="<?= Url::toRoute('/' . $layout .  'profile'); ?>">
                        <img src="<?= \common\models\User::getAvatar($id); ?>" alt="Avatar" />
                    </a>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </header>
    <section id="main_content" class="clearfix">
        <?= Alert::widget() ?>
        <?= $content ?>
        <section id="chat"  class="<?= Yii::$app->user->isGuest ? '' : 'logged'; ?> ">
            <header class="chat_header clearfix">
                <h4><?= \yii::t('app', 'Чат. ') . \common\models\Dcchats::getUsersOnline() . \yii::t('app',' онлайн'); ?></h4>
                <div class="chat_button_holder">
                    <span class="button minimize">
                        <i class="fa fa-plus"></i>
                        <i class="fa fa-minus"></i>
                    </span>
                    <span class="button modal_btn">
                        <i class="fa fa-arrow-down"></i>
                        <i class="fa fa-arrow-up"></i>
                    </span>
                </div>
            </header>
            <section class="chat_content" id="chat_content" data-url="<?= Url::toRoute($homePath . '/refreshchat'); ?>">
                <div class="action_block">
                    <a class="action_link" href="<?= Url::toRoute('pages/rules');  ?>"><?= \yii::t('app', 'Правила');?></a>
                    <a class="action_link" href="/<?= $layout ?>support#faq"><?= \yii::t('app', 'Помощь');?></a>
                    <?php if(!Yii::$app->user->isGuest): ?>
                        <a class="action_link" href="javascript:" id="change_nickname" style="<?= is_null(Yii::$app->session->get('nickname')) ? 'display: none' : ''; ?>">
                            <?= \yii::t('app', 'Сменить ник');?>
                        </a>
                        <a class="action_link login" href="javascript:" id="join_chat" style="<?= !is_null(Yii::$app->session->get('nickname')) ? 'display: none' : ''; ?>">
                            <?= \yii::t('app', 'Войти');?>
                        </a>
                    <?php endif; ?>
                </div>
                <?= $this->render('_messagesD', [
                    'layout' => $layout,
                ]); ?>

            </section>
            <footer class="input_holder">
              <?php $user = \common\models\User::getCurrentUser();
              if($user) {
                  if($user->chat==\common\models\User::STATUS_NOT_BANNED):
              ?>
                <form id="msg_form">
                    <input name="message" id="msg_input" style="<?= is_null(Yii::$app->session->get('nickname')) ? 'display: none' : ''; ?>" data-url="<?= Url::toRoute($homePath . '/sendmsg'); ?>" />
                </form>
                <?php endif;
              }
              ?>
                <span data-nickname="<?= Yii::$app->session->get('nickname'); ?>" id="curr_nickname"></span>
                <input name="user_nickname" id="user_nickname"  style="display: none" />
        <span class="button" id="accept_nickname" style="cursor: pointer; display: none" data-url="<?= Url::toRoute($homePath . '/setnickname'); ?>">
                <i class="fa fa-plus"> join</i>
            </span>
            </footer>
        </section>
    </section>
</div>

    <footer id="footer">
        <div class="container">
            <div class="footer_lists_holder clearfix">
                <div class="footer_list_block">
                    <h3><?= \yii::t('app', 'О сайте'); ?></h3>
                    <ul>
                        <li><a href="<?= Url::toRoute(['/' . $layout . 'pages', 'view'=>'about']); ?>"><?= \yii::t('app', 'Как работает система?'); ?></a></li>
                        <li><a href="/<?= $layout ?>support#faq"><?= \yii::t('app', 'Часто задаваемые вопросы'); ?></a></li>
                        <li><a href="/<?= $layout ?>profile"><?= \yii::t('app', 'Ввод/вывод средств'); ?></a></li>
                        <li><a href="<?= Url::toRoute('/' . $layout . 'support'); ?>"><?= \yii::t('app', 'Техподдержка'); ?></a></li>
                    </ul>
                </div>
                <div class="footer_list_block">
                    <h3><?= \yii::t('app', 'Магазин'); ?></h3>
                    <ul>
                        <li><a href="<?= Url::toRoute('/' . $layout); ?>"><?= \yii::t('app', 'Предложения'); ?></a></li>
                        <li><a href="<?= Url::toRoute('/' . $layout . 'profile'); ?>"><?= \yii::t('app', 'Инвентарь'); ?></a></li>

                        <li><a href="<?= Url::toRoute('/' . $homePath . '/discount'); ?>"><?= \yii::t('app', 'Скидки'); ?></a></li>
                    </ul>
                </div>
                <div class="footer_list_block">
                    <h3><?= \yii::t('app', 'Соцсети'); ?></h3>
                    <ul>
                        <li>
                            <a href="<?= Settings::find()->one()->vk_dota; ?>" target="_blank">
                                <i class="fa fa-vk"></i>
                                <span><?= \yii::t('app', 'Dota2'); ?></span>
                            </a>
                        </li>
                        <li>
                            <a href="<?= Settings::find()->one()->vk_cs; ?>" target="_blank">
                                <i class="fa fa-vk"></i>
                                <span><?= \yii::t('app', 'CS:GO'); ?></span>
                            </a>
                        </li>
                        <li>
                            <a href="<?= Settings::find()->one()->youtube; ?>" target="_blank">
                                <i class="fa fa-youtube-play"></i>
                                <span><?= \yii::t('app', 'YouTube'); ?></span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="payment_block clearfix">
                    <h3><?= \yii::t('app', 'Мы принимаем'); ?></h3>
                    <div class="systems_holder">
                        <img src="/images/data/pp.png" alt="PayPal Money"/>
                        <img src="/images/data/visa_wallet.jpg" alt="Visa Wallet"/>
                        <img src="/images/data/web_money.jpg" alt="Web Money"/>
                    </div>
                </div>
            </div>
            <div class="copyright">
                <span>&copy; 2016 Dota2</span>
            </div>
            <!-- Разработка: angeluss и  medusa. Верстка: crazy_squirrel. -->
            <!-- © Shark IT -->
        </div>
    </footer>
</section>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
