<?php $layout = isset($this->params['layout']) ? $this->params['layout'] : ''; ?>
<?php $homePath = $layout === '' ? 'site' : $layout; ?>
<?php $user = \common\models\User::getCurrentUser(); ?>
<?php foreach(\common\models\Notifications::getAllUserNotifications($user->id) as $notification): ?>
    <li>
        <div class="not-block">
            <p>
                <?= date('Y-m-d H:i:s', $notification->created) ?>
                <a class="close_notification" href="javascript:" data-url="<?= \yii\helpers\Url::toRoute($homePath . '/mark-notification-as-read')?>" data-id="<?= $notification->id ;?>">
                    <i class="fa fa-close" aria-hidden="true"></i>
                </a>
            </p>
            <p class="text"><?= $notification->text; ?></p>
        </div>
    </li>
<?php endforeach; ?>
<?php if (count(\common\models\Notifications::getAllUserNotifications($user->id)) == 0): ?>
    <li>
        <?= yii::t('app', 'Нет новых сообщений')?>
    </li>
<?php endif; ?>