<h1><?= \common\models\News::getDefTitle($new->id)?></h1>
<?php if(!is_null($new->image_path)) : ?>
    <div class="content">
        <img src="<?= '/uploads/'.$new->image_path ?>" />
    </div>
<?php endif; ?>
<p><?= \common\models\News::getContent($new->id)?></p>