
<section id="main_content" class="clearfix">
<div class="content_holder news_list_holder">
<h1>Следим за групповым этапом The Shanghai Major (UPD: 26.02)</h1>
<div class="meta">
    <a class="count_comments" href="">
        <i class="fa fa-comment"></i>
        <span>56</span>
    </a>
    <a class="user" href="">
        <i class="fa fa-user"></i>
        <span>Korb3n</span>
    </a>
    <a class="data" href="">
        <i class="fa fa-calendar"></i>
        <span>21 февраля 2016 в 18:15</span>
    </a>
    <div class="tags">
        <span><i class="fa fa-tags"></i></span>
        <ul>
            <li><a href="">pudge,</a></li>
            <li><a href="">dota2,</a></li>
            <li><a href="">конкурс,</a></li>
            <li><a href="">dota</a></li>
        </ul>
    </div>
</div>
<div class="news_article">
<div class="article_content">
    <p>Уже сегодня ночью, 25 февраля, стартует главный ивент этой зимы - The Shanghai Major. По 28
        ферваля пройдет групповой этап, который распеделит 16 команд по верхней и нижней сетке плей-офф
        стадии майн ивента</p>
    <p>
        <img src="images/data/news_5.jpg" />
    </p>
    <p>В данной новости вы сможете следить за ходом событий в групповом этапе <strong>The Shanghai Major
        </strong>, который будет проходить <strong>c 25 по 28 февраля</strong></p>
    <h5>Формат группового этапа:</h5>
    <ul>
        <li>4 группы по 4 команды</li>
        <li>Игры в группе будут проходить по схеме сетки double-elimination</li>
        <li>Все игры best-of-3</li>
        <li>Первые 2 места группы попадают в верхнюю сетку, последние 2 места попадают в нижную сетку
            мейн-ивента</li>
    </ul>
    <p>Более подробная информация о самом турнире <a href="">здесь</a>.</p>
</div>
<div class="action_block clearfix">
    <div class="rate_holder">
        <div class="title">
            <h5>Оцените пост</h5>
        </div>
        <div class="rate">
                            <span class="change">
                                <i class="fa fa-plus"></i>
                            </span>
            <span class="count">328</span>
                            <span class="change minus">
                                <i class="fa fa-minus"></i>
                            </span>
        </div>
    </div>
    <div class="social_block">
        <div class="title">
            <h5>Поделитесь с друзьями</h5>
        </div>
        <div class="social">
            <ul>
                <li>
                    <a class="vk" href=""></a>
                </li>
                <li>
                    <a class="fb" href=""></a>
                </li>
                <li>
                    <a class="tw" href=""></a>
                </li>
                <li>
                    <a class="google" href=""></a>
                </li>
                <li>
                    <a class="mail_ru" href=""></a>
                </li>
                <li>
                    <a class="email" href=""></a>
                </li>
                <li>
                    <a class="add" href=""></a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="news_top_holder similar_news">
    <div class="image_news_holder clearfix">
        <div class="image_news">
            <div class="image_block">
                <img src="images/data/news_top_2.jpg" />
            </div>
            <div class="news_desc">
                <div class="meta">
                    <a class="count_comments" href="">
                        <i class="fa fa-comment"></i>
                        <span>56</span>
                    </a>
                    <a class="user" href="">
                        <i class="fa fa-user"></i>
                        <span>Korb3n</span>
                    </a>
                </div>
                <h3 class="news_title">Следим за групповым этапом The Shanghai Major (UPD: 26.02)</h3>
            </div>
        </div>
        <div class="image_news">
            <div class="image_block">
                <img src="images/data/news_top_2.jpg" />
            </div>
            <div class="news_desc">
                <div class="meta">
                    <a class="count_comments" href="">
                        <i class="fa fa-comment"></i>
                        <span>56</span>
                    </a>
                    <a class="user" href="">
                        <i class="fa fa-user"></i>
                        <span>Korb3n</span>
                    </a>
                </div>
                <h3 class="news_title">Следим за групповым этапом The Shanghai Major (UPD: 26.02)</h3>
            </div>
        </div>
        <div class="image_news">
            <div class="image_block">
                <img src="images/data/news_top_2.jpg" />
            </div>
            <div class="news_desc">
                <div class="meta">
                    <a class="count_comments" href="">
                        <i class="fa fa-comment"></i>
                        <span>56</span>
                    </a>
                    <a class="user" href="">
                        <i class="fa fa-user"></i>
                        <span>Korb3n</span>
                    </a>
                </div>
                <h3 class="news_title">Следим за групповым этапом The Shanghai Major (UPD: 26.02)</h3>
            </div>
        </div>
        <div class="image_news">
            <div class="image_block">
                <img src="images/data/news_top_2.jpg" />
            </div>
            <div class="news_desc">
                <div class="meta">
                    <a class="count_comments" href="">
                        <i class="fa fa-comment"></i>
                        <span>56</span>
                    </a>
                    <a class="user" href="">
                        <i class="fa fa-user"></i>
                        <span>Korb3n</span>
                    </a>
                </div>
                <h3 class="news_title">Следим за групповым этапом The Shanghai Major (UPD: 26.02)</h3>
            </div>
        </div>
        <div class="image_news">
            <div class="image_block">
                <img src="images/data/news_top_2.jpg" />
            </div>
            <div class="news_desc">
                <div class="meta">
                    <a class="count_comments" href="">
                        <i class="fa fa-comment"></i>
                        <span>56</span>
                    </a>
                    <a class="user" href="">
                        <i class="fa fa-user"></i>
                        <span>Korb3n</span>
                    </a>
                </div>
                <h3 class="news_title">Следим за групповым этапом The Shanghai Major (UPD: 26.02)</h3>
            </div>
        </div>
        <div class="image_news">
            <div class="image_block">
                <img src="images/data/news_top_2.jpg" />
            </div>
            <div class="news_desc">
                <div class="meta">
                    <a class="count_comments" href="">
                        <i class="fa fa-comment"></i>
                        <span>56</span>
                    </a>
                    <a class="user" href="">
                        <i class="fa fa-user"></i>
                        <span>Korb3n</span>
                    </a>
                </div>
                <h3 class="news_title">Следим за групповым этапом The Shanghai Major (UPD: 26.02)</h3>
            </div>
        </div>
    </div>
</div>

<div class="comments_holder">
    <h3>Комментарии</h3>
    <div class="comment clearfix">
        <div class="karma">
                            <span class="change">
                                <i class="fa fa-plus"></i>
                            </span>
            <span class="count">3</span>
                            <span class="change minus">
                                <i class="fa fa-minus"></i>
                            </span>
        </div>
        <div class="comment_content">
            <div class="comment_title clearfix">
                <div class="image_block">
                    <img src="images/data/avatar.jpg" />
                </div>
                <h5>Steam Steamovigh</h5>
                                <span class="date">
                                    <i class="fa fa-calendar"></i>
                                    Вчера 14:50
                                </span>
            </div>
            <div class="desc">
                <p>МВП то неплохи</p>
            </div>
        </div>
    </div>
    <div class="comment clearfix">
        <div class="karma">
                            <span class="change">
                                <i class="fa fa-plus"></i>
                            </span>
            <span class="count">3</span>
                            <span class="change minus">
                                <i class="fa fa-minus"></i>
                            </span>
        </div>
        <div class="comment_content">
            <div class="comment_title clearfix">
                <div class="image_block">
                    <img src="images/data/avatar.jpg" />
                </div>
                <h5>Steam Steamovigh</h5>
                                <span class="date">
                                    <i class="fa fa-calendar"></i>
                                    Вчера 14:50
                                </span>
            </div>
            <div class="desc">
                <p>МВП то неплохи</p>
            </div>
        </div>
    </div>
    <div class="comment answer clearfix">
        <div class="karma">
                            <span class="change">
                                <i class="fa fa-plus"></i>
                            </span>
            <span class="count">3</span>
                            <span class="change minus">
                                <i class="fa fa-minus"></i>
                            </span>
        </div>
        <div class="comment_content">
            <div class="comment_title clearfix">
                <div class="image_block">
                    <img src="images/data/avatar.jpg" />
                </div>
                <h5>Steam Steamovigh</h5>
                                <span class="date">
                                    <i class="fa fa-calendar"></i>
                                    Вчера 14:50
                                </span>
            </div>
            <div class="desc">
                <p>МВП то неплохи</p>
            </div>
        </div>
    </div>
</div>

<!-- ERROR COMMENT -->
<!--<div class="comment_form error">
    <div class="image_block">
        <i class="fa fa-exclamation-triangle"></i>
    </div>
    <div class="error_desc">
        <h3>Пожалуйста, авторизуйтесь!</h3>
        <p>Чтобы оставить комментарий необходимо авторизоваться на сайте Steam - это просто и абсолютно
        безопасно</p>
    </div>
</div>-->

<div class="comment_form">
    <div class="title">
        <h5>Оставте свой комментарий</h5>
    </div>
    <form id="comment">
        <div class="form">
            <label for="comment_text">Комментарий</label>
            <textarea id="comment_text" name="comment"></textarea>
            <button class="button">Добавить комментарий</button>
        </div>
    </form>
</div>
</div>
</div>
<aside class="right_sidebar">
    <h2>Популярное в ленте</h2>
    <div class="list_block">
        <div class="title">
            <h3>Лучшее</h3>
                    <span class="sorter_link">
                        <a href="">за неделю</a>
                    </span>
                    <span class="right_sorter_link">
                        <a href="">Топ-50</a>
                    </span>
        </div>
        <ul class="news_list">
            <li>
                <div class="karma">
                    <span>+327</span>
                </div>
                <div class="list_content">
                    <h5 class="list_title">
                        <a href="">Итоги "Зимнего шмотопада" в Январе</a>
                    </h5>
                    <div class="meta">
                        <a class="count_comments" href="">
                            <i class="fa fa-comment"></i>
                            <span>56</span>
                        </a>
                        <a class="user" href="">
                            <i class="fa fa-user"></i>
                            <span>Korb3n</span>
                        </a>
                        <a class="data" href="">
                            <i class="fa fa-calendar"></i>
                            <span>21 февраля 2016 в 18:15</span>
                        </a>
                    </div>
                </div>
            </li>
            <li>
                <div class="karma">
                    <span>+87</span>
                </div>
                <div class="list_content">
                    <h5 class="list_title">
                        <a href="">Простейший профит в Стиме без вкладов реала. Блог Сообщества Трейдеров.</a>
                    </h5>
                    <div class="meta">
                        <a class="count_comments" href="">
                            <i class="fa fa-comment"></i>
                            <span>56</span>
                        </a>
                        <a class="user" href="">
                            <i class="fa fa-user"></i>
                            <span>Korb3n</span>
                        </a>
                        <a class="data" href="">
                            <i class="fa fa-calendar"></i>
                            <span>21 февраля 2016 в 18:15</span>
                        </a>
                    </div>
                </div>
            </li>
            <li>
                <div class="karma">
                    <span>+61</span>
                </div>
                <div class="list_content">
                    <h5 class="list_title">
                        <a href="">Обновление Dota 2 от 22.02.2016: патч 6.86f</a>
                    </h5>
                    <div class="meta">
                        <a class="count_comments" href="">
                            <i class="fa fa-comment"></i>
                            <span>56</span>
                        </a>
                        <a class="user" href="">
                            <i class="fa fa-user"></i>
                            <span>Korb3n</span>
                        </a>
                        <a class="data" href="">
                            <i class="fa fa-calendar"></i>
                            <span>21 февраля 2016 в 18:15</span>
                        </a>
                    </div>
                </div>
            </li>
        </ul>
    </div>

    <div class="list_block">
        <div class="title">
            <h3>Обсуждаемое</h3>
                    <span class="sorter_link">
                        <a href="">за неделю</a>
                    </span>
                    <span class="right_sorter_link">
                        <a href="">Топ-50</a>
                    </span>
        </div>
        <ul class="news_list">
            <li>
                <div class="karma">
                    <span>+327</span>
                </div>
                <div class="list_content">
                    <h5 class="list_title">
                        <a href="">Итоги "Зимнего шмотопада" в Январе</a>
                    </h5>
                    <div class="meta">
                        <a class="count_comments" href="">
                            <i class="fa fa-comment"></i>
                            <span>56</span>
                        </a>
                        <a class="user" href="">
                            <i class="fa fa-user"></i>
                            <span>Korb3n</span>
                        </a>
                        <a class="data" href="">
                            <i class="fa fa-calendar"></i>
                            <span>21 февраля 2016 в 18:15</span>
                        </a>
                    </div>
                </div>
            </li>
            <li>
                <div class="karma">
                    <span>+87</span>
                </div>
                <div class="list_content">
                    <h5 class="list_title">
                        <a href="">Простейший профит в Стиме без вкладов реала. Блог Сообщества Трейдеров.</a>
                    </h5>
                    <div class="meta">
                        <a class="count_comments" href="">
                            <i class="fa fa-comment"></i>
                            <span>56</span>
                        </a>
                        <a class="user" href="">
                            <i class="fa fa-user"></i>
                            <span>Korb3n</span>
                        </a>
                        <a class="data" href="">
                            <i class="fa fa-calendar"></i>
                            <span>21 февраля 2016 в 18:15</span>
                        </a>
                    </div>
                </div>
            </li>
            <li>
                <div class="karma">
                    <span>+61</span>
                </div>
                <div class="list_content">
                    <h5 class="list_title">
                        <a href="">Обновление Dota 2 от 22.02.2016: патч 6.86f</a>
                    </h5>
                    <div class="meta">
                        <a class="count_comments" href="">
                            <i class="fa fa-comment"></i>
                            <span>56</span>
                        </a>
                        <a class="user" href="">
                            <i class="fa fa-user"></i>
                            <span>Korb3n</span>
                        </a>
                        <a class="data" href="">
                            <i class="fa fa-calendar"></i>
                            <span>21 февраля 2016 в 18:15</span>
                        </a>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</aside>
</section>
