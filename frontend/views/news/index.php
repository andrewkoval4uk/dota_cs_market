<?php
$params = Yii::$app->request->get();
/* @var $this yii\web\View */
$layout = isset($this->params['layout']) ? $this->params['layout'] : '';
$homePath = $layout === '' ? 'site' : $layout;
 $user = \common\models\User::getCurrentUser();
?>
<div class="content_holder news_list_holder">
<h1><?= \yii::t('app', 'Новости ') ?></h1>
<nav class="sorter_holder">
   <ul class="sorter">
        <li class="<?= (!isset($params['rubric'])) ? 'active' : ''?>"><a href="<?=\yii\helpers\Url::toRoute('/' . $layout . 'news'); ?>">Последние</a></li>
        <?php foreach($rubrics as $r):
            ?>
            <li class="<?= ((isset($params['rubric'])) && $params['rubric']== $r['id']) ? 'active' : ''?>" >
                <a href="<?=\yii\helpers\Url::toRoute(['/' . $layout . 'news', 'rubric' => $r['id']]); ?> "><?= $r['name_ru'] ?></a></li>
        <?php endforeach ?>
        <li class="<?= ((isset($params['rubric'])) && $params['rubric'] == 'all') ? 'active' : ''?>">
            <a href="<?= \yii\helpers\Url::toRoute(['/' . $layout . 'news', 'rubric' => 'all']); ?>">Все новости</a>
        </li>
    </ul>
</nav>
<?php if($flag_for_sort) : ?>
    <div class="news_top_holder">
        <div class="image_news full_images">
            <?php if (!is_null($generalNew)): ?>
                <a href="<?= \Yii::$app->urlManager->createUrl([$layout . 'news/view', 'id' => $generalNew->id]); ?>">
                    <div class="image_block">
                            <img src="<?=  !is_null($generalNew->image_path) ? 'uploads/'. $generalNew->image_path : 'uploads/top_def.jpg'; ?>" alt="<?= \common\models\News::getDefTitle($generalNew->id)?>" />
                    </div>
                </a>
                <div class="news_desc">
                    <div class="meta">
                        <a class="count_comments" href="">
                            <i class="fa fa-comment"></i>
                            <span><?= $generalNew->comments ?></span>
                        </a>
                        <a class="user" href="">
        <!--                    <i class="fa fa-user"></i>-->
        <!--                    <span>Korb3n</span>-->
                        </a>
                        <a class="data" href="">
                            <i class="fa fa-calendar"></i>
                            <span><?= date('d/m/Y H:i:s', $generalNew->created_at); ?></span>
                        </a>
                    </div>
                    <a href="<?= \Yii::$app->urlManager->createUrl([$layout . 'news/view', 'id' => $generalNew->id]); ?>">
                        <h3 class="news_title"><?= \common\models\News::getDefTitle($generalNew->id)?></h3>
                    </a>
                </div>
            <?php endif; ?>
        </div>
        <div class="image_news_holder clearfix">
            <?php foreach($topNews as $top) : ?>
                <a href="<?= \Yii::$app->urlManager->createUrl([$layout . 'news/view', 'id' => $top['id']]); ?>">
                    <div class="image_news">
                        <div class="image_block">
                            <img src="<?=  !is_null($top['image_path']) ? 'uploads/'. $top['image_path'] : 'uploads/default.jpg'; ?>" alt="" />
                        </div>
                        <div class="news_desc">
                            <div class="meta">
                                <a class="count_comments" href="">
                                    <i class="fa fa-comment"></i>
                                    <span><?= $top['comments']?> </span>
                                </a>
                                <a class="user" href="">
                                    <!--                        <i class="fa fa-user"></i>-->
                                    <!--                        <span>Korb3n</span>-->
                                </a>
                            </div>
                            <a href="<?= \Yii::$app->urlManager->createUrl([$layout . 'news/view', 'id' => $top['id']]); ?>">
                                <h3 class="news_title"><?= \common\models\News::getDefTitle($top['id'])?></h3>
                            </a>
                        </div>
                    </div>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>

<div class="news_list">
    <?php foreach ($news as $new) :?>
    <div class="news clearfix">
        <div class="karma">
            <span class="change" onclick="setLikes(<?= $new['id'] ?>, 'plus', <?= $new['likes'] ?>, false)">
                <i class="fa fa-plus"></i>
            </span>
            <span class="count" id="count_for_new<?= $new['id']?>"><?= $new['likes'] >= 0 ? '+' . $new['likes']  : $new['likes'] ?></span>
            <span class="change minus" onclick="setLikes(<?= $new['id'] ?>, 'minus', <?= $new['likes'] ?>, false )">
                <i class="fa fa-minus"></i>
            </span>
        </div>
        <div class="news_content clearfix">
            <div class="image_block">
                <img src="<?= !is_null($new['image_path']) ? 'uploads/'. $new['image_path'] : 'uploads/default.jpg'; ?>" alt="" />
            </div>
            <div class="news_desc">
                <a href="<?= \Yii::$app->urlManager->createUrl([$layout . 'news/view', 'id' => $new['id']]); ?>">
                    <h3 class="news_title"><?= \common\models\News::getDefTitle($new['id']) ?></h3>
                    <?php if(!is_null($new['image_path'])) : ?>
                        <div class="content">
                            <img src="<?= '/uploads/' . $new['image_path'] ?>" />
                        </div>
                    <?php endif; ?>
                </a>
                <div class="meta">
                    <a class="count_comments" href="">
                        <i class="fa fa-comment"></i>
                        <span><?= $new['comments'] ?></span>
                    </a>
                    <a class="user" href="">
    <!--                    <i class="fa fa-user"></i>-->
    <!--                    <span>Korb3n</span>-->
                    </a>
                    <a class="data" href="javascript:">
                        <i class="fa fa-calendar"></i>
                        <span><?= date('d/m/Y H:i:s', $new['created_at']); ?></span>
                    </a>
                    <div class="tags">
                        <?php if(isset($new->tags)) { ?>
                            <span><i class="fa fa-tags"></i></span>
                            <ul>
                             <?php foreach ($new->tags as $t ) : ?>
                                    <li><a href="<?=\yii\helpers\Url::toRoute(['/' . $layout . 'news', 'tag' => $t->id]); ?>"><?= $t->name; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                         <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>
<!--<div class="more_btn">-->
<!--    <button>Показать еще</button>-->
<!--</div>-->
</div>
<aside class="right_sidebar">
    <h2>Популярное в ленте</h2>
    <div class="list_block">
        <div class="title">
            <h3>Лучшее</h3>
                    <span class="sorter_link">
                        <a href="<?=\yii\helpers\Url::toRoute(['/' . $layout . 'news',['sort'=>'best-week']]); ?>" onclick="BestWeekNews()">за неделю</a>
                    </span>
                    <span class="right_sorter_link">
                        <a href="<?=\yii\helpers\Url::toRoute(['/' . $layout . 'news',['sort'=>'best-top50']]); ?>">Топ-50</a>
                    </span>
        </div>
        <ul id="bestNews" class="news_list">
            <?php
            $bestNews = \common\models\News::getBestNews();
            foreach($bestNews as $best) : ?>
            <li>
                <div class="karma">
                    <span><?= $best['likes']>=0 ? '+' . $best['likes']  :  $best['likes'] ?></span>
                </div>
                <div class="list_content">
                    <h5 class="list_title">
                      <a href="<?=\Yii::$app->urlManager->createUrl([$layout . 'news/view', 'id' => $best['id']])?>">
                          <?=\common\models\News::getDefTitle($best['id'])?></a>
                    </h5>
                    <div class="meta">
                        <a class="count_comments" href="">
                            <i class="fa fa-comment"></i>
                            <span><?= $best['comments']?></span>
                        </a>
                        <a class="user" href="">
<!--                            <i class="fa fa-user"></i>-->
<!--                            <span>Korb3n</span>-->
                        </a>
                        <a class="data" href="">
                            <i class="fa fa-calendar"></i>
                            <span><?= date('d/m/Y H:i:s', $best['created_at']); ?></span>
                        </a>
                    </div>
                </div>
            </li>
            <?php endforeach; ?>
        </ul>

        <ul id="bestWeekNews" class="news_list" style="display: none">
            <?php
            $bestNews = \common\models\News::getBestWeekNews();
            foreach($bestNews as $best) : ?>
                <li>
                    <div class="karma">
                        <span><?= $best['likes']>=0 ? '+'.$best['likes']  : $best['likes'] ?></span>
                    </div>
                    <div class="list_content">
                        <h5 class="list_title">
                            <a href="<?=\Yii::$app->urlManager->createUrl([$layout . 'news/view', 'id' => $best['id']])?>">
                                <?=\common\models\News::getDefTitle($best['id'])?></a>
                        </h5>
                        <div class="meta">
                            <a class="count_comments" href="">
                                <i class="fa fa-comment"></i>
                                <span><?= $best['comments']?></span>
                            </a>
                            <a class="user" href="">
                                <!--                            <i class="fa fa-user"></i>-->
                                <!--                            <span>Korb3n</span>-->
                            </a>
                            <a class="data" href="">
                                <i class="fa fa-calendar"></i>
                                <span><?= date('d/m/Y H:i:s', $best['created_at']); ?></span>
                            </a>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>

    </div>

    <div class="list_block">
        <div class="title">
            <h3>Обсуждаемое</h3>
             <span class="sorter_link">
                        <a href="<?=\yii\helpers\Url::toRoute(['/' . $layout . 'news',['sort' => 'discuss-week']]); ?>" onclick="BestWeekNews()">за неделю</a>
                    </span>
                    <span class="right_sorter_link">
                        <a href="<?=\yii\helpers\Url::toRoute(['/' . $layout . 'news',['sort' => 'best-discuss50']]); ?>">Топ-50</a>
                    </span>
        </div>
        <ul class="news_list">
            <?php foreach($moreComments as $com) : ?>
                <li>
                    <div class="karma">
                        <span><?= $com['likes']>=0 ? '+'.$com['likes']  :  $com['likes'] ?></span>
                    </div>
                    <div class="list_content">
                        <h5 class="list_title">
                            <a href="<?=\Yii::$app->urlManager->createUrl([$layout . 'news/view', 'id' => $com['id']])?>">
                                <?=\common\models\News::getDefTitle($com['id'])?></a>
                        </h5>
                        <div class="meta">
                            <a class="count_comments" href="">
                                <i class="fa fa-comment"></i>
                                <span><?= $com['comments']?></span>
                            </a>
                            <a class="user" href="">
                                <!--                            <i class="fa fa-user"></i>-->
                                <!--                            <span>Korb3n</span>-->
                            </a>
                            <a class="data" href="">
                                <i class="fa fa-calendar"></i>
                                <span><?= date('d/m/Y H:i:s', $com['created_at']); ?></span>
                            </a>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</aside>
