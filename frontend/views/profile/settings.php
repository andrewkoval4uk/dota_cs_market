<div class="main clearfix">
    <div class="content_holder account">
        <h1><?= yii::t('app', 'Настройки'); ?></h1>
        <div class="error_block error">
            <div class="image_block">
                <i class="fa fa-exclamation-triangle"></i>
            </div>
            <div class="error_desc">
                <h3><?= yii::t('app', 'Остерегайтесь мошенников!'); ?></h3>
                <p><?= yii::t('app', 'Чтобы не стать жертвой мошенничества, никогда не сообщайте посторонним людям вашу ссылку на обмен!'); ?></p>
            </div>
        </div>
        <div class="settings_block">
            <div class="input_holder">
                <div class="label_holder">
                    <h5><?= yii::t('app', 'Ваша ссылка на обмен'); ?></h5>
                    <a target="_blank" href="http://steamcommunity.com/profiles/<?= Yii::$app->getUser()->getIdentity()->profile['name']; ?>/tradeoffers/privacy" title="">
                        <?= yii::t('app', 'Узнать тут'); ?>
                    </a>
                </div>
                <input id="link_for_swap" type="text" value="<?= $model->exchange_link ?>" name="link_for_swap" />
            </div>
        </div>
<!--        <div class="settings_block">-->
<!--            <div class="row">-->
<!--                <h5>Уведомления о включении покупок и продаж</h5>-->
<!--                <div class="radio_holder">-->
<!--                    <input id="show_everywhere" type="radio" value="1" name="all_pages" />-->
<!--                    <label for="show_everywhere">Показывать на всех страницах</label>-->
<!---->
<!--                    <input value="0" id="show_once" type="radio" name="all_pages" --><?php //= !$model->all_pages ? 'checked' : '' ?><!-- />-->
<!--                    <label for="show_once">Показывать только не сранице продажи</label>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row">-->
<!--                <h5>Использовать таймер для влкючения продаж</h5>-->
<!--                <div class="radio_holder">-->
<!--                    <input id="auto_switch" type="radio" value="1" name="timer" --><?php //= $model->timer ? 'checked' : '' ?><!--  />-->
<!--                    <label for="auto_switch">Включать через 15 секунд</label>-->
<!---->
<!--                    <input id="no_switch" value="0" type="radio" name="timer" --><?php //= !$model->timer ? 'checked' : '' ?><!-- />-->
<!--                    <label for="no_switch">Не включать автоматически</label>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
        <div class="settings_block">
            <div class="row">
                <h5><?= yii::t('app', 'Настройка опевещений через браузер'); ?></h5>
                <p><?= yii::t('app', 'При покупке Вашей вещи, появится окно в правом нижнем углу монитора (даже если браузер
                    свернут в трей) и прозвучит разовый звуковой сигнал. Если у Вас будет закрыта с нашим
                    сайтом, то оповещения не произойдет.'); ?></p>
                <div class="button_holder">
                    <button data-value="<?= $model->browser ?>" id="enabled_push_notifications" class="button <?= $model->browser ? 'btn-primary' : ''?> "
                            style="cursor: pointer"><?= $model->browser ? yii::t('app', 'Выключить оповещения через браузер') : yii::t('app', 'Включить оповещения через браузер') ; ?></button>
                </div>
            </div>
<!--            <div class="row">-->
<!--                <h5>Настройка оповещений по e-mail</h5>-->
<!--                <div class="input_holder">-->
<!--                    <label for="email">Укажите ваш электронный адрес:</label>-->
<!--                    <input id="email" type="email" value="" name="link_for_swap" />-->
<!--                    <button onclick="comingSoon()" class="button">Выслать код подтверждения</button>-->
<!--                </div>-->
<!--            </div>-->

        </div>
    </div>
    <?= $this->render('parts/right_menu', []) ?>
</div>

<?= $this->render('parts/buy_history', []) ?>