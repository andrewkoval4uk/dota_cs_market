<div class="main clearfix">
    <div class="content_holder account">
        <h1>История тикетов техподдержки</h1>

     <?php
     if(isset($tickets)) {
     foreach($tickets as $ticket) : ?>
         <div class="panel panel-default <?= $ticket->status ==1 ? 'panel-success':'panel-danger' ?>">
             <div class="panel-heading">
                 <h3 class="panel-title"><?= $ticket->getThemeName()?></h3>
             </div>
             <div class="panel-body">
                 <?= $ticket->text?>
             </div>
             <?php
             if(isset($ticket->messages)){ ?>
                 <ul class="list-group">
                      <?php foreach ($ticket->messages as $m) { ?>

                                 <li class="list-group-item"><p>
                                         <?= $m->sender==2 ? 'Админ' :'Вы' ?></p><?=$m->text ?></li>

                       <?php  }  } ?>
                     <li class="list-group-item">
                         <div class="form-group">
                             <div id="" style="width: 70px;" onclick="showTicketMessageForm(<?= $ticket->id ?>)" class="btn button btn-primary">Ответить</div>
                         </div>
                     </li>
                     <li data-id="<?= $ticket->id ?>" class="list-group-item wrapper-tickets-messages-<?= $ticket->id ?>" style="display: none">
                         <?php $form = \yii\widgets\ActiveForm::begin([
                             'id' => 'messages_tickets_form',
                             'options' => ['class' => 'form-horizontal'],
                         ]) ?>
                         <div class="form">
                             <?= $form->field($newMessage, 'text')->textarea(['id'=>'messages-text'])->label(false) ?>
                             <?= $form->field($ticket, 'id')->hiddenInput(['id'=>'messages-text', 'class'=>'hidden' ])->label(false) ?>
                             <div class="form-group">
                                     <?= \yii\helpers\Html::submitButton('Добавить сообщение', ['class' => 'btn button btn-primary']) ?>
                             </div>
                         </div>
                         <?php \yii\widgets\ActiveForm::end() ?>
                     </li>

                 </ul>


         </div>
        <?php
     endforeach;
     } else { ?>
         <p> Не найдено тикетов. Создать тикет <a href="<?=\yii\helpers\Url::toRoute(['/support']); ?>">здесь</a></p>
    <?php }?>
        <p> Создать новый тикет <a href="<?=\yii\helpers\Url::toRoute(['/support']); ?>">тут</a></p>
    </div>
    <?= $this->render('parts/right_menu', []) ?>
</div>

<?= $this->render('parts/buy_history', []) ?>
