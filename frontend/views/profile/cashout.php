<div class="main clearfix">
    <div class="content_holder account">
        <h1>Ввод/вывод средств</h1>
        <div class="support_form with_margin">
            <div class="title">
                <h5>Пополнение личного счета</h5>
            </div>
            <form id="input_money" method="post">
                <div class="form">
                    <div class="input_holder">
                        <span>1. Выберите платежную систему</span>
                        <div class="radio_holder">
                            <input id="yandex" type="radio" name="payment_method" checked value="<?= \common\models\Proposal::YANDEX_METHOD?>" />
                            <label for="yandex"></label>
                        </div>
                        <div class="radio_holder">
                            <input id="visa" type="radio" name="payment_method" value="<?= \common\models\Proposal::QIWI_METHOD?>" />
                            <label for="visa"></label>
                        </div>
                        <div class="radio_holder">
                            <input id="web_money" type="radio" name="payment_method" value="<?= \common\models\Proposal::WM_METHOD?>" />
                            <label for="web_money"></label>
                        </div>
                    </div>
                    <div class="input_holder">
                        <label for="money_input">2. Введите сумму, на которую вы хотите пополнить ваш личный
                            счет, и нажмите на кнопку "Пополнить". Вы будете переадресованы на сайт платежной
                            системы, где вы сможете завершить платеж</label>
                        <input id="money_input" type="text" name="money_input" value="" />
                        <button class="button">Пополнить</button>
                    </div>
                </div>
            </form>
        </div>

        <div class="support_form with_margin">
            <div class="title">
                <h5>Вывод денег со счета</h5>
            </div>
            <form id="output_money" method="post">
                <div class="form">
                    <div class="info">
                        <h6>Правила вывода средств из системы</h6>
                        <ul>
                            <li>1. Вывод производится на кошелек к WebMoney, QIWI или Яндекс.Деньги. Вывод на другие
                                платежные системы не производится.</li>
                            <li>2. В случае указания неправильного номера кошелька средства не возвращаются.</li>
                            <li>3. Процесс обработки заявки на вывод обычно занимает менее часа, но в некоторых ситуациях
                                может занять до 3 рабочих дней.</li>
                        </ul>
                    </div>
                    <h6>Вывести средства:</h6>
                    <div class="input_holder">
                        <span>1. Выберите платежную систему</span>
                        <div class="radio_holder">
                            <input id="output_yandex" type="radio" name="Proposal[payment]" value="<?= \common\models\Proposal::YANDEX_METHOD?>" checked />
                            <label for="output_yandex"></label>
                        </div>
                        <div class="radio_holder">
                            <input id="output_visa" type="radio" name="Proposal[payment]"  value="<?= \common\models\Proposal::QIWI_METHOD?>" />
                            <label for="output_visa"></label>
                        </div>
                        <div class="radio_holder">
                            <input id="output_web_money" type="radio" name="Proposal[payment]"  value="<?= \common\models\Proposal::WM_METHOD?>"/>
                            <label for="output_web_money"></label>
                        </div>
                    </div>
                    <div class="input_holder inline">
                        <label for="account">2. Введите ваш номер счета:</label>
                        <input id="account" type="text" name="Proposal[account]" value="" />
                    </div>
                    <div class="input_holder inline">
                        <label for="money_output">2. Укажите сумму для вывода:</label>
                        <input id="money_output" type="text" name="Proposal[amount]" value="0" />
                    </div>
                    <div class="input_holder inline">
                        <label for="total">Сумма с учетом комиссии 5%:</label>
                        <input id="total" type="text" name="amount" value="0" readonly="readonly"/>
                    </div>
                    <div class="input_holder inline">
                        <span>Итого к получению:</span>
                        <span class="price"><span id="total2">0</span> p</span>
                    </div>
                    <div class="checkbox_holder">
                        <input id="tickets-checkbox" type="checkbox" name="agree" />
                        <label for="tickets-checkbox">Я подтверждаю правильность введенных данных и согласен с условиями</label>
                    </div>
                    <div class="input_holder">
                        <button class="button" id="create_ticket_btn" disabled="disabled">Вывести</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?= $this->render('parts/right_menu', []) ?>
</div>

<?= $this->render('parts/buy_history', []) ?>