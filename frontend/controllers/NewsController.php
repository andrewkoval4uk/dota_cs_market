<?php

namespace frontend\controllers;

use common\models\Comments;
use common\models\CommentsLikesState;
use common\models\News;
use common\models\NewsLikesState;
use common\models\Rubrics;
use common\models\Tags;
use common\models\User;
use Yii;

class NewsController extends \yii\web\Controller
{

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }


    public function actionIndex()
    {
        $params = Yii::$app->request->get();
        $generalNew = News::getGeneralNew();
        $topNews = News::getTopNews();
        $flag_for_sort = 1;
        $news = News::getNews();

        if(!is_null ($params)) {

            if(isset($params['rubric'])){
                if($params['rubric'] !== 'all') {
                   $rub_id = $params['rubric'];
                   $news_collection = Rubrics::getRubricsNews($rub_id);
                   if($news_collection[0]['news']) {
                       $news = $news_collection[0]['news'];
                       $flag_for_sort = 0;
                   }
                }
            }

            if(isset($params['tag'])){
                $tag_id = $params['tag'];
                $news_collection = Tags::getNewsByTag($tag_id);
                if($news_collection[0]['news']) {
                    $news = $news_collection[0]['news'];
                    $flag_for_sort = 0;
                }
            }

            if(isset($params[1]['sort'])){
                $param_value = $params[1]['sort'];

                switch($param_value){
                    case 'best-week':
                        break;
                    case 'best-top50':
                        $flag_for_sort = 0;
                        $news=News::getNewsTopByGet('likes');
                        break;
                    case 'discuss-week':
                        break;

                    case 'best-discuss50':
                        $flag_for_sort = 0;
                        $news=News::getNewsTopByGet('comments');
                        break;
                }
            }
        }

        $rubrics = Rubrics::find()->where(['status' => News::STATUS_ACTIVE])->all();
        $bestNews = News::getBestNews(); //@TODO что за параметр?
        $bestWeek = News::getBestWeekNews(); //@TODO что за параметр?
        $bestComments = News::getBestComments();
        $weekComments = News::getWeekComments();

        return $this->render('index', [
            'topNews'       => $topNews,
            'news'          => $news,
            'rubrics'       => $rubrics,
            'moreComments'  => $bestComments,
            'generalNew'    => $generalNew,
            'weekComments'  => $weekComments,
            'flag_for_sort' => $flag_for_sort,
        ]);
    }

    public function actionView($id)
    {
        $comments = new Comments();
        $params = Yii::$app->request->post();

        if(!is_null ($params)) {
            if(isset($params['Comments'])){
                $comments_par = $params['Comments'];
                $comments->message = $comments_par['message'];
                $comments->new_id = $id;
                $identity = Yii::$app->getUser()->getIdentity();
                $id_user = User::getUserIdBySteam($identity->profile['name']);
                $comments->user_id = $id_user;
                $comments->created_at = time();
                if($comments->save()) {
                    $this->refresh();
                }
            }
        }
        $similar_news = News::getSimilarNews();
        $comments_list = News::getCommentsList($id);
        $new = News::getNew($id);
        $bestComments = News::getBestComments();

        $params = Yii::$app->request->get();

        return $this->render('view', [
            'new'           => $new,
            'moreComments'  => $bestComments,
            'comments'      => $comments,
            'comments_list' => $comments_list,
            'similar_news'  => $similar_news,
            'params'        => $params,
        ]);
    }

    public function actionSetlikes()
    {
        if (Yii::$app->request->isAjax) {
            if ($data = Yii::$app->request->post()) {

                //@TODO убрать этот костыль и передавать нормальный $data["comments"]
                if($data["comments"] == 'false'){
                    $data["comments"] = false;
                }
                if(!$data['comments']) {

                    $likes_state = new NewsLikesState;
                    $entity = News::findOne($data['id']);
                    $where_key = 'new_id';
                    $likes_state->new_id = $entity->id;

                } else {
                    $likes_state = new CommentsLikesState;
                    $entity = Comments::findOne($data['id']);
                    $where_key = 'comment_id';
                    $likes_state->comment_id = $entity->id;

                }
                if($likes_state_model = $likes_state::find([$where_key=>$entity->id, 'user_id'=>Yii::$app->user->id])->one()) {
                    if ((!$likes_state_model->positive) && $data['operation'] == 'plus') {
                        $likes_state_model->positive = 1;
                        $likes_state_model->negative =0;
                        $entity->likes = $data['likes'] + 1;
                    }
                    if((!$likes_state_model->negative) && $data['operation'] == 'minus' ) {
                        $likes_state_model->positive = 0;
                        $likes_state_model->negative =1;
                        $entity->likes = $data['likes'] - 1;
                    }
                    $likes_state->user_id = Yii::$app->user->id;

                    if($likes_state_model->save()) {

                    } else {

                    }
                }
                else {

                    if ($data['operation'] == 'plus') {
                        $likes_state->positive = 1;
                        $likes_state->negative = 0;
                        $entity->likes = $data['likes'] + 1;
                    }
                    if($data['operation'] == 'minus' ) {
                        $likes_state->negative =1;
                        $likes_state->positive = 0;
                        $entity->likes = $data['likes'] - 1;
                    }
                    $likes_state->user_id = Yii::$app->user->id;

                   if($likes_state->save()) {

                   }

                }

                if($entity->save()) {
                    $likes = $entity->likes;
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return $likes;
                } else {
                    return 'error';
                }
            }
        }
    }

}
