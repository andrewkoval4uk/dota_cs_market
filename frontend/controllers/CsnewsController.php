<?php

namespace frontend\controllers;

use Yii;
use yii\base\ViewContextInterface;

class CsnewsController extends NewsController implements ViewContextInterface
{

    public function beforeAction($action) {
        $this->view->params['layout'] = 'cs';
        Yii::$app->session->set('layout', 'cs');
        return parent::beforeAction($action);
    }

    public function getViewPath()
    {
        return Yii::getAlias('@frontend/views/news');
    }

}
