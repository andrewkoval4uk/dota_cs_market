<?php
namespace frontend\controllers;

use common\models\Commisions;
use common\models\Dcchats;
use common\models\DiscountPurchases;
use common\models\Notifications;
use common\models\Pages;
use common\models\PagesLang;
use common\models\Trades;
use common\models\User;
use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class SiteController extends Controller
{

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        if ($action->id=='error'){
            $url = explode('/', Yii::$app->request->url);
           if(strpos($url[1], 'cs') === 0){
               $this->view->params['layout'] = 'cs';
           }
        }
        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'eauth' => array(
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => array('login'),
            ),
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'setnickname', 'markNotificationAsRead'],
                'rules' => [
                    [
                        'actions' => ['signup', 'setnickname', 'MarkNotificationAsRead'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'setnickname', 'MarkNotificationAsRead'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $items = \common\models\Trades::find()
            ->orderBy([
                'amount' => SORT_DESC
            ])
            ->all();
        return $this->render('index', [
            'items' => $items,
        ]);
    }

    /**
     * Displays statistic page.
     *
     * @return mixed
     */
    public function actionStatistics()
    {
        $expensive_day = Trades::find()
            ->orderBy([
                'amount' => SORT_DESC
            ])
            ->Where('date >= DATE_SUB(CURRENT_DATE, INTERVAL 1 DAY)')
            ->limit(8)
            ->all();
        $last = Trades::find()
            ->orderBy([
                'date' => SORT_DESC
            ])
            ->limit(16)
            ->all();
        $expensive_all = Trades::find()
            ->orderBy([
                'amount' => SORT_DESC
            ])
            ->limit(8)
            ->all();
        return $this->render('statistic', [
            'last' => $last,
            'expensive_day' => $expensive_day,
            'expensive_all' => $expensive_all,
        ]);
    }
    public function actionAbout()
    {
        $last = 'Hello';

        return $this->render('about', [
            'last' => $last
        ]);
    }

    public function actionDiscount(){
        $id = Yii::$app->request->get('id');
        $discounts = DiscountPurchases::find()->all();
        $comissions = Commisions::find()->all();

       if(isset($id)) {
           $discount_user = User::getUserDiscount($id);
           $trades = User::getBuyerTrades($id);
           $params = User::getDiscountByTrades($trades,$discounts);
           $discount_trade =$params['discount'];
           $amount = $params['amount'];
           $real_discount = $discount_user>=$discount_trade ? $discount_user : $discount_trade;
       } else {
           $amount = 0;
           $real_discount = 0;
       }

        return $this->render('discount', [
            'discounts' => $discounts,
            'comissions' => $comissions,
            'real_discount' => $real_discount,
            'amount'=>$amount
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
//    public function actionLogin()
//    {
//        if (!\Yii::$app->user->isGuest) {
//            return $this->goHome();
//        }
//
//        $model = new LoginForm();
//        if ($model->load(Yii::$app->request->post()) && $model->login()) {
//            return $this->goBack();
//        } else {
//            return $this->render('login', [
//                'model' => $model,
//            ]);
//        }
//    }

    public function actionLogin() {
        $serviceName = Yii::$app->getRequest()->getQueryParam('service');
        if (isset($serviceName)) {
            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
            $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
            $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('site/login'));

            try {
                if ($eauth->authenticate()) {
//                  var_dump($eauth->getIsAuthenticated(), $eauth->getAttributes()); exit;

                    $identity = User::findByEAuth($eauth);
                    Yii::$app->getUser()->login($identity);

                    // special redirect with closing popup window
                    $eauth->redirect();
                }
                else {
                    // close popup window and redirect to cancelUrl
                    $eauth->cancel();
                }
            }
            catch (\nodge\eauth\ErrorException $e) {
                // save error to show it later
                Yii::$app->getSession()->setFlash('error', 'EAuthException: '.$e->getMessage());

                // close popup window and redirect to cancelUrl
//              $eauth->cancel();
                $eauth->redirect($eauth->getCancelUrl());
            }
        }

        // default authorization code through login/password ..
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionSetnickname(){

        if (Yii::$app->request->isAjax) {
            $nickname = Yii::$app->request->post('nick', false);
            if ($nickname) {
                Yii::$app->session->set('nickname', $nickname);
                $success = true;
            } else {
                $success = false;
            }
            return Json::encode(array(
                'success' => $success,
            ));
        } else {
            die('error');
        }
    }

    public function actionRefreshchat(){
        if (Yii::$app->request->isAjax) {
//            $msg = Yii::$app->request->post('msg', false);
//            $nickname = Yii::$app->request->post('nick', false);
            $this->layout=false;
            $html = $this->render('../layouts/_messagesD');
            return Json::encode(array(
                'success' => true,
                'html' => $html,
            ));
        } else {
            die('error');
        }
    }
    public function actionSetreport(){
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id', false);
            $model = Dcchats::findOne($id);
            $model->status = Dcchats::STATUS_REPORT;
            if ($model->save()) {
              return $success = true;
            } else {
                VarDumper::dump($model->errors);
            }

        }
    }
    public function actionSendmsg(){
        if (Yii::$app->request->isAjax) {
            $msg = Yii::$app->request->post('msg', false);
            $nickname = Yii::$app->request->post('nick', false);

            $model = new Dcchats();
            $model->nickname = $nickname;
            $model->message = Dcchats::parseMsg($msg);
            $model->status = Dcchats::STATUS_NORM;
            $model->date = time();
            $string_array = explode('id/', Yii::$app->user->id);
            $model->sender = (int)$string_array[2];

            if ($model->save()) {
                $success = true;
            } else {
                VarDumper::dump($model->errors);
                echo $model->message;
                $success = false;
            }

            return Json::encode(array(
                'success' => $success,
            ));
        } else {
            die('error');
        }
    }

    public function actionMarkNotificationAsRead(){

        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id', false);

            $model = Notifications::findOne($id);
            if(is_null($model)){
                $success = false;
            } else {
                $model->status = Notifications::STATUS_READ;
                if($model->save()){
                    $success = true;
                } else {
                    VarDumper::dump($model->errors); die;
                }
            }

            $this->layout = false;
            $html = $this->render('../layouts/_notifications');
            $user = User::getCurrentUser();
            $count = count(\common\models\Notifications::getAllUserNotifications($user->id));

            return Json::encode(array(
                'success' => $success,
                'html' => $html,
                'count' => $count,
            ));
        } else {
            die('error');
        }
    }

    public function actionRefreshinv(){

        if (Yii::$app->request->isAjax) {
            $gameID = Yii::$app->request->post('game', false);
            $userSteamId = Yii::$app->getUser()->getIdentity()->profile['name'];
            $userInv = \frontend\models\SteamService::getUserInventory($userSteamId, $gameID, \frontend\models\SteamService::$inventoryId);
            $userInv = json_decode($userInv);
            if (isset($userInv->rgInventory) && isset($userInv->rgDescriptions)) {
                $items = \frontend\models\SteamService::getItemsInventoryDescription($userInv->rgInventory, $userInv->rgDescriptions);
            } else {
                $items = false;
            }
            $this->layout = false;
            $html = $this->render('../profile/parts/_inventory', ['items' => $items]);

            return Json::encode(array(
                'success' => true,
                'html' => $html,
            ));
        } else {
            die('error');
        }
    }

    public function actionStopmytrades(){
        $user = \common\models\User::getCurrentUser();
        $items = Trades::find()
            ->where('status = :status and seller_id = :id', ['status' => Trades::STATUS_PROGRESS, 'id' => $user->id])
            ->orderBy([
                'amount' => SORT_DESC
            ])
            ->all();

        foreach($items as $i){
            $i->delete();
        }
        return Json::encode(array(
            'success' => true,
        ));
    }

}
