<?php
namespace frontend\controllers;

use common\models\Messages;
use common\models\Pages;
use common\models\PagesLang;
use common\models\Proposal;
use common\models\SettingsProfile;
use common\models\Tickets;
use common\models\Trades;
use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use  \common\models\User;

/**
 * Site controller
 */
class ProfileController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $user = \common\models\User::getCurrentUser();
        $items = Trades::find(['buyer_id' => $user->id])
            ->where('buyer_id = :id or seller_id = :id', ['id' => $user->id])
            ->orderBy([
                'amount' => SORT_DESC
            ])
            ->all();

        $active_items = Trades::find()
            ->where('status = :status and seller_id = :id', ['status' => Trades::STATUS_PROGRESS, 'id' => $user->id])
            ->orderBy([
                'amount' => SORT_DESC
            ])
            ->all();

        return $this->render('index', [
            'user' => $user,
            'self_items' => $items,
            'active_trades' => $active_items,
        ]);
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionSettings()
    {
        $setting = SettingsProfile::find(['user_id'=>Yii::$app->user->id])->one();
        if (!$setting) {
            $setting = new SettingsProfile;
        }
        return $this->render('settings', [
            'model' => $setting

        ]);
    }

    public function  actionSavesettings() {
        $this->enableCsrfValidation = false;

        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $id = Yii::$app->user->id;
           if(is_null($id)) {
               $id = 0;
           }
            $settings = SettingsProfile::find(['user_id'=>Yii::$app->user->id])->one();
            if(!isset($settings)){
                $settings = new SettingsProfile();
                $settings->user_id = $id;
            }
            if($data['name'] == 'all_pages') {
                $settings->all_pages = $data['value'];
            }
            if ($data['name'] == 'timer'){
                $settings->timer = $data['value'];
            }
            if($data['name'] == 'save_link') {
                $settings->exchange_link = $data['value'];
            }
            if($data['name'] == 'save_browser') {
                $settings->browser = $data['value'] == 0 ? 1 : 0;
            }
            if ($settings->save()){

                return 'success';
            } else {

               var_dump($settings->errors);
            }

        }
    }

    public function actionCashout()
    {
        $id = Yii::$app->request->get('id');
        $model = new Proposal();
        $model->user_id = $id;
        $model->date = time();
        $payment = Yii::$app->request->post('payment_method', false);
        if($payment !== false){
            $sum = Yii::$app->request->post('money_input');
            switch($payment) {
                case Proposal::QIWI_METHOD:
                    return $this->render('qiwi', [
                        'sum' => $sum,
                    ]);
                case Proposal::YANDEX_METHOD:
                    return $this->render('yandex', [
                        'sum' => $sum,
                    ]);
                case Proposal::WM_METHOD:
                    return $this->render('webmoney', [
                        'sum' => $sum,
                    ]);
            }
        }
//        VarDumper::dump(Yii::$app->request->post()); die;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', 'Заявка успешно создана');
        }

        return $this->render('cashout');
    }

    public function actionSupport()
    {
        $id = Yii::$app->request->get('id');
        if(isset($id)) {
            $user_id = $id;
        }
        else {
            $user_id=0;
        }
        $newMessage = new Messages();
        if (Yii::$app->request->post()) {

            $message = Yii::$app->request->post('Messages');
            $ticket = Yii::$app->request->post('Tickets');
            $newMessage->ticket_id = $ticket['id'];
            $newMessage->text = $message['text'];
            $newMessage->sender = 1;
            $newMessage->date = time();
             if($newMessage->save()){
                 $this->refresh();
             }
        }
        $tickets = Tickets::find(['user_id'=>$user_id])->with('messages')->orderBy('created_at')->all();
        return $this->render('support', [
            'tickets'=>$tickets,
            'newMessage'=>$newMessage
        ]);
    }

    public function actionRequests(){
        $user = \common\models\User::getCurrentUser();
        $requests = Proposal::findAll(['user_id' => $user->id]);

        return $this->render('requests', [
            'requests' => $requests,
        ]);
    }

    public function actionSuccesspayment(){
        $user = User::getCurrentUser();
        $user->balance += Yii::$app->request->get('amount');
        if($user->save()){
            \Yii::$app->session->setFlash('success', 'Счет успешно пополнен');
        } else {
            \Yii::$app->session->setFlash('error', 'Произошла ошибка. Пожалуйста, проверьте реквизиты или попробуйте еще раз');
        }
        return $this->redirect(['cashout', 'id' => $user->id]);
    }

}
