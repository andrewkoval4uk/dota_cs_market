<?php

namespace frontend\controllers;

use common\models\Pages;
use common\models\PagesLang;
use yii\web\HttpException;
use yii\base\ViewContextInterface;
use Yii;

class CspagesController extends PagesController implements ViewContextInterface
{

    public function beforeAction($action) {
        $this->view->params['layout'] = 'cs';
        Yii::$app->session->set('layout', 'cs');
        return parent::beforeAction($action);
    }

    public function getViewPath()
    {
        return Yii::getAlias('@frontend/views/pages');
    }

}
