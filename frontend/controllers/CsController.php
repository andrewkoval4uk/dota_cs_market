<?php
namespace frontend\controllers;

use common\models\Commisions;
use common\models\Cschats;
use common\models\Dcchats;
use common\models\DiscountPurchases;
use common\models\Pages;
use common\models\PagesLang;
use common\models\User;
use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\base\ViewContextInterface;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class CsController extends SiteController implements ViewContextInterface
{

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        $this->view->params['layout'] = 'cs';
        Yii::$app->session->set('layout', 'cs');
        return parent::beforeAction($action);
    }

    public function getViewPath()
    {
        return Yii::getAlias('@frontend/views/site');
    }

    public function actionAbout()
    {
        $last = 'Hello';

        return $this->render('about', [
            'last' => $last
        ]);
    }


    public function actionLogin() {
        $serviceName = Yii::$app->getRequest()->getQueryParam('service');
        if (isset($serviceName)) {
            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
            $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
            $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('site/login'));

            try {
                if ($eauth->authenticate()) {
//                  var_dump($eauth->getIsAuthenticated(), $eauth->getAttributes()); exit;

                    $identity = User::findByEAuth($eauth);
                    Yii::$app->getUser()->login($identity);

                    // special redirect with closing popup window
                    $eauth->redirect('/cs');
                }
                else {
                    // close popup window and redirect to cancelUrl
                    $eauth->cancel();
                }
            }
            catch (\nodge\eauth\ErrorException $e) {
                // save error to show it later
                Yii::$app->getSession()->setFlash('error', 'EAuthException: '.$e->getMessage());

                // close popup window and redirect to cancelUrl
//              $eauth->cancel();
                $eauth->redirect($eauth->getCancelUrl());
            }
        }

        // default authorization code through login/password ..
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return  Yii::$app->getResponse()->redirect('/cs');
    }

    public function actionRefreshchat(){
        if (Yii::$app->request->isAjax) {
            $this->layout=false;
            $html = $this->render('../layouts/_messagesD', [
                'layout' => 'cs',
            ]);
            return Json::encode(array(
                'success' => true,
                'html' => $html,
            ));
        } else {
            die('error');
        }
    }

    public function actionSendmsg(){
        if (Yii::$app->request->isAjax) {
            $msg = Yii::$app->request->post('msg', false);
            $nickname = Yii::$app->request->post('nick', false);

            $model = new Cschats();
            $model->nickname = $nickname;
            $model->message = Dcchats::parseMsg($msg);
            $model->status = Dcchats::STATUS_NORM;
            $model->date = time();

            if ($model->save()) {
                $success = true;
            } else {
                VarDumper::dump($model->errors);
                echo $model->message;
                $success = false;
            }

            return Json::encode(array(
                'success' => $success,
            ));
        } else {
            die('error');
        }
    }

}
