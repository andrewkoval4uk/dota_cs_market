/**
 * Created by julia on 3/26/16.
 */

   function setLikes(new_id, operation, likes, comments) {
    var wrapper = jQuery('#count_for_new'+new_id);
    var data =  {id : new_id, 'operation' : operation, 'likes' : likes, 'comments' : comments };
    $.ajax({
        url: '/news/setlikes', //@TODO you should get url via data-attributes
        type:"POST",
        dataType: "json",
        data:data,
        success: function(data) {
            if (data>0) {
                wrapper.html('+'+data);
            }
            if (data<0) {
                wrapper.html(data);
            }
        },
        error: function () {}
    });
}
function showTicketMessageForm(id){
    $('.wrapper-tickets-messages-'+id).toggle();
}
function comingSoon() {
    alert('В разработке');
}
function BestWeekNews() {
    jQuery('#bestNews').hide();
    jQuery('#bestWeekNews').show();
}

function refreshChat(){
    var url = $('#chat_content').data('url');

    $.ajax({
        url : url,
        type : "POST",
        dataType : "json",
        data : {

        },
        success: function (data) {
            if(data.success) {
                //$('#message_block').empty().append(data.html);
                //$('#chat_content').animate({ scrollTop: $('#chat_content').prop("scrollHeight")}, 100);
                var div = $('#chat_content');
                //console.log($(div).scrollTop() + $(div).innerHeight());
                //console.log($(div)[0].scrollHeight);
                if( $(div).scrollTop() + $(div).innerHeight() >= $(div)[0].scrollHeight ) {
                    $('#message_block').empty().append(data.html);
                    $(div).animate({ scrollTop: $(div).prop("scrollHeight")}, 100);
                } else {
                    $('#message_block').empty().append(data.html);
                }
            } else {
                alert('Ошибка. Пожалуйста, попробуйте позже. Если ошибка повторится, обратитесь к администратору')
            }
        }
    });
}
function saveSettingsFunction(value, name) {
    var data =  {value : value, 'name' : name};
    $.ajax({
        url: '/profile/savesettings',
        type:"POST",
        dataType: "json",
        data:data,
        success: function(data) {

        },
        error: function () {}
    });
}
function setReportStatusToMessage(id) {
    var url = $('#chat-report-button-'+id).data('url');
    var data =  {id : id};
    $.ajax({
        url: url,
        type:"POST",
        dataType: "json",
        data:data,
        success: function(data) {
            alert('Администратор рассмотрит Вашу жалобу');
        },
        error: function () {}
    });
}
$(document).ready(function() {
    $('#link_for_swap').keyup(function(){
        if(event.keyCode==13)
        {
            var value = this.value;
            var name =  'save_link';

            saveSettingsFunction(value, name);
        }
    });
    $('#enabled_push_notifications').click(function(){

            var value = $('#enabled_push_notifications').data('value');
            var name =  'save_browser';
            saveSettingsFunction(value, name);
             location.reload();
    });
    //$('#chat_content').animate({ scrollTop: $('#chat_content').prop("scrollHeight")}, 100);
    $('input[type=radio][name=all_pages]').change(function() {
      var value = this.value;
      var name =  'all_pages';

      saveSettingsFunction(value, name);

    });
    $('input[type=radio][name=timer]').change(function() {
        var value = this.value;
        var name =  'timer';

        saveSettingsFunction(value, name);

    });

    $('#join_chat').on('click', function(){
        $('#user_nickname').css('display', 'inline-block');
        $('#accept_nickname').css('display', 'inline-block');
        $('#msg_input').css('display', 'none');
    });
    $('.social li a').on('click', function(){
       alert('В разработке');
    });
    $('#accept_nickname').on('click', function() {
        var url = $(this).data('url');
        var nick = $('#user_nickname').val();
        if( nick.length > 2 ) {
            $.ajax({
                url : url,
                type : "POST",
                dataType : "json",
                data : {
                    nick: nick
                },
                success: function (data) {
                    if(data.success) {
                        $('#user_nickname').css('display', 'none');
                        $('#accept_nickname').css('display', 'none');
                        $('#join_chat').css('display', 'none');
                        $('#change_nickname').css('display', 'inline-block');
                        $('#msg_input').css('display', 'inline-block');
                        $('#curr_nickname').data('nickname', nick);
                    } else {
                        alert('Ошибка. Пожалуйста, попробуйте позже. Если ошибка повторится, обратитесь к администратору')
                    }
                }
            });
        } else {
            alert('В нике должно быть минимум 3 символа');
        }
    });
    $('#change_nickname').on('click', function(){
        $('#user_nickname').css('display', 'inline-block');
        $('#accept_nickname').css('display', 'inline-block');
        $('#msg_input').css('display', 'none');
    });

    setInterval(function() { refreshChat() }, 3000);

    $('#msg_form').on('submit', function(e) {
        e.preventDefault();

        var nick = $('#curr_nickname').data('nickname');
        var msg = $('#msg_input').val();
        var url = $('#msg_input').data('url');

        $.ajax({
            url : url,
            type : "POST",
            dataType : "json",
            data : {
                nick: nick,
                msg: msg
            },
            success: function (data) {
                if(data.success) {
                    refreshChat();
                    $('#msg_input').val('');
                    $('#chat_content').animate({ scrollTop: $('#chat_content').prop("scrollHeight")}, 100);
                } else {
                    alert('Ошибка. Пожалуйста, попробуйте позже. Если ошибка повторится, обратитесь к администратору')
                }
            }
        });
    });

    $('#tickets-theme_id').on('change', function() {
        var val = $(this).val();
        if(val == 0){
            $('#tickets-other_theme').css('display', 'block');
        } else {
            $('#tickets-other_theme').css('display', 'none');
        }
    });
    $('.show-notification-icon').on('click', function(){
        $('#notifications_block').toggle();
    });
    $('#tickets-checkbox').on('change', function(){
        var val = $(this).prop('checked');
        if(val === false ){
            $('#create_ticket_btn').attr('disabled', 'disabled');
        } else {
            $('#create_ticket_btn').removeAttr('disabled');
        }
    });
    $('#overlay').fadeIn(400,
        function(){
            $('#popup-stop-trade')
                .css('display', 'block')
                .animate({opacity: 1, top: '50%'}, 200);
        });

    $('.close-modal-stop-trade, #overlay, #close_notification_btn').click( function(e){
        e.preventDefault();
        $('#popup-stop-trade')
            .animate({opacity: 0, top: '45%'}, 200,
            function(){
                $(this).css('display', 'none');
                $('#overlay').fadeOut(400);
            }
        );
    });

    $('#money_output').on('keyup', function(){
        var amount = $(this).val();
        amount = 0.95* amount;
        amount.toFixed(2);
        if(amount > 0){
            $('#total').val(amount);
            $('#total2').html(amount);
        }
    });

    $('.dataTables-table').DataTable( {
        "language": {
            "lengthMenu": "Показать _MENU_ записей на странице",
            "zeroRecords": "Пока нет записей",
            "info": "Показана _PAGE_ страница из _PAGES_",
            "infoEmpty": "Недоступно",
            "infoFiltered": "(отобрано из _MAX_ записей)",
            "search": "Поиск",
            "paginate": {
                "first":      "Первая",
                "last":       "Последняя",
                "next":       "Следующая",
                "previous":   "Предыдущая"
            }
        }
    } );

    $('.close_notification').on('click', function(){
        var url = $(this).data('url');
        var id = $(this).data('id');

        $.ajax({
            url : url,
            type : "POST",
            dataType : "json",
            data : {
                id: id
            },
            success: function (data) {
                if(data.success) {
                    $('#notifications_block').empty().append(data.html);
                    $('#notifications_count').html(data.count);
                } else {
                    alert('Ошибка. Пожалуйста, попробуйте позже. Если ошибка повторится, обратитесь к администратору')
                }
            }
        });
    });

    $('.add_good').on('click', function(){
        $('.all_items').css('display', 'block');
    });

    $('.hide_goods').on('click', function(){
        $('.all_items').css('display', 'none');
    });
    $('.modal-window .item.good').on('click', function(){
        var name = $(this).find('.item_name').html();
        $('#tarhet_good_name').html(name);
        var good_photo = $(this).find('img').prop('src');
        $('#target_good_src').prop('src', good_photo);
        $('.modal-item-view').show();
    });
    $('.modal-item-view .hide_goods').on('click', function(){
        $('.modal-window').hide();
    });

    $('.modal-item-view .hide_good_prop').on('click', function(){
        $('.start_trade').hide();
    });

    $( "#spinner" ).spinner({ min: 0, step: 1 });

    $('#spinner').keypress(function(e) {
        if (e.keyCode < 48 || e.keyCode > 57) {
            return false;
        }
    });
    google.charts.load('current', {'packages':['corechart', 'line']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Data', 'Позавчера', 'Вчера', 'Сегодня'],
            ['0:00',     0,      250, 500],
            ['3:00',  1600,      460, 750],
            ['6:00',  660,       1120, 389],
            ['9:00',  1030,      540, 485],
            ['12:00',  0,      0, 0],
            ['15:00',  666,      4, 3],
            ['18:00',  234,      643, 456],
            ['21:00',  232,      4, 7]
        ]);

        var options = {
            legend: { position: 'top' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
    }

    $('#refresh_inventory').on('click', function(){
        var url = $(this).data('url');
        var game = $(this).data('game');

        $.ajax({
            url : url,
            type : "POST",
            dataType : "json",
            data : {
                game: game
            },
            success: function (data) {
                if(data.success) {
                    $('#inventory').empty().append(data.html);
                } else {
                    alert('Ошибка. Пожалуйста, попробуйте позже. Если ошибка повторится, обратитесь к администратору')
                }
            }
        });
    });

    $('#stop_my_trades').on('click', function(){
        var url = $(this).data('url');
        if (confirm("Вы уверены?")) {

            $.ajax({
                url : url,
                type : "POST",
                dataType : "json",
                success: function (data) {
                    location.reload();
                }
            });
        }
    });

});
