<?php

use yii\db\Schema;
use yii\db\Migration;

class m160227_234253_create_chats extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('dcchats', [
            'id' => $this->primaryKey(),
            'nickname' => $this->string()->notNull(),
            'message' => $this->text(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'date' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('cschats', [
            'id' => $this->primaryKey(),
            'nickname' => $this->string()->notNull(),
            'message' => $this->text(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'date' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('dcchats');
        $this->dropTable('cschats');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
