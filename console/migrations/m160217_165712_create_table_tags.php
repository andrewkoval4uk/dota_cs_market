<?php

use yii\db\Schema;
use yii\db\Migration;

class m160217_165712_create_table_tags extends Migration
{
    public function up()
    {
        $this->createTable('tags', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'status' => Schema::TYPE_BOOLEAN . ' DEFAULT 0',
        ]);
    }

    public function down()
    {
        $this->dropTable('tags');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
