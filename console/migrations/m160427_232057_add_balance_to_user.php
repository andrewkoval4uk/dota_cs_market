<?php

use yii\db\Migration;

class m160427_232057_add_balance_to_user extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'balance', $this->integer() . '  NOT NULL DEFAULT 0' );
    }

    public function down()
    {
        $this->dropColumn('user', 'balance');
    }
}
