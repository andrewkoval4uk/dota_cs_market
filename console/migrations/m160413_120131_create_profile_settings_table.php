<?php

use yii\db\Migration;

class m160413_120131_create_profile_settings_table extends Migration
{
    public function up()
    {
        $this->createTable('profile_settings_table', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'exchange_link' => $this->string()->notNull(),
            'timer' => $this->integer()->notNull(),
            'all_pages' => $this->integer()->notNull(),
            'browser' => $this->integer()->notNull(),
            'email' => $this->string()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('profile_settings_table');
    }
}
