<?php

use yii\db\Schema;
use yii\db\Migration;

class m160413_010040_add_themes_to_tickets extends Migration
{
    public function up()
    {
        $this->execute("
        ALTER TABLE `tickets` ADD `theme_id` INT(11) NOT NULL DEFAULT '0' AFTER `status`, ADD `other_theme` VARCHAR(255) NOT NULL DEFAULT ' ' AFTER `theme_id`;
        ");
    }

    public function down()
    {
        $this->execute("
            ALTER TABLE `tickets`
            DROP `theme_id`,
            DROP `other_theme`;
        ");

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
