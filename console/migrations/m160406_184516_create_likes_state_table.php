<?php

use yii\db\Schema;
use yii\db\Migration;

class m160406_184516_create_likes_state_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('likes_state', [

            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'new_id' => $this->integer()->notNull(),
            'positive' => $this->integer()->notNull(),
            'negative' => $this->integer()->notNull(),

        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('likes_state');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
