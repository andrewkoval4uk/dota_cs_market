<?php

use yii\db\Schema;
use yii\db\Migration;

class m160227_211958_create_table_settings extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'meta_title' => $this->string()->notNull()->defaultValue(''),
            'admin_email' => $this->string()->notNull()->defaultValue(''),
            'support_email' => $this->string()->notNull()->defaultValue(''),
            'skype' => $this->string()->notNull()->defaultValue(''),
            'ya_acc' => $this->string()->notNull()->defaultValue(''),
            'qiwi_acc' => $this->string()->notNull()->defaultValue(''),
            'wm_acc' => $this->string()->notNull()->defaultValue(''),
            'percent' => $this->string()->notNull()->defaultValue(''),
            'vk_dota' => $this->string()->notNull()->defaultValue(''),
            'vk_cs' => $this->string()->notNull()->defaultValue(''),
            'youtube' => $this->string()->notNull()->defaultValue(''),

        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('settings');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
