<?php

use yii\db\Migration;

class m160507_114142_add_date_to_proposal extends Migration
{
    public function up()
    {
        $this->addColumn('proposal', 'date', $this->string() . '  NOT NULL' );
    }

    public function down()
    {
        $this->dropColumn('proposal', 'date');
    }
}
