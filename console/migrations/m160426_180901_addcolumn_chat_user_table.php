<?php

use yii\db\Migration;

class m160426_180901_addcolumn_chat_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'chat', \yii\db\oci\Schema::TYPE_INTEGER . ' DEFAULT 1');
    }

    public function down()
    {
        $this->dropColumn('user', 'chat');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
