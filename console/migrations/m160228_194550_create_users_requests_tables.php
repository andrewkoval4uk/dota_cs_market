<?php

use yii\db\Schema;
use yii\db\Migration;

class m160228_194550_create_users_requests_tables extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('user_request', [
            'id' => $this->primaryKey(),
            'id_request' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(),
            'amount' => $this->integer()->notNull(),
        ], $tableOptions);


    }

    public function down()
    {
        $this->dropTable('user_request');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
