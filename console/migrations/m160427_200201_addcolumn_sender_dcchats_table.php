<?php

use yii\db\Migration;

class m160427_200201_addcolumn_sender_dcchats_table extends Migration
{
    public function up()
    {
        $this->addColumn('dcchats', 'sender', $this->bigInteger(50) . '  NOT NULL' );
    }

    public function down()
    {
        $this->dropColumn('dcchats', 'sender');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
