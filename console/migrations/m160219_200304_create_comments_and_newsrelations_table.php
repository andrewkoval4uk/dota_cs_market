<?php

use yii\db\Schema;
use yii\db\Migration;

class m160219_200304_create_comments_and_newsrelations_table extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('news_lang', [
            'id' => $this->primaryKey(),
            'new_id' => $this->integer(),
            'lang' => $this->string(),
            'title' => $this->string(),
            'content' => $this->text(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
        ], $tableOptions);

        $this->createTable('comments', [
            'id' => $this->primaryKey(),
            'new_id' => $this->integer(),
            'user_id' => $this->integer(),
            'message' => $this->text(),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->createTable('tags_news', [
            'id' => $this->primaryKey(),
            'new_id' => $this->integer(),
            'tag_id' => $this->integer(),
        ], $tableOptions);

        $this->createTable('rubrics_news', [
            'id' => $this->primaryKey(),
            'new_id' => $this->integer(),
            'rubric_id' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('rubrics_news');
        $this->dropTable('tags_news');
        $this->dropTable('comments');
        $this->dropTable('news_lang');
    }


    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {

    }

    public function safeDown()
    {
    }
}
