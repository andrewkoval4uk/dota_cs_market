<?php

use yii\db\Migration;

class m160421_202003_alter_column_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'steamId', $this->bigInteger(50) . '  NOT NULL' );
    }

    public function down()
    {
        $this->dropColumn('user', 'steamId');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
