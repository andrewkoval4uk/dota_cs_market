<?php

use yii\db\Schema;
use yii\db\Migration;

class m160330_194934_addcolumn_likes_comments_table extends Migration
{
    public function up()
    {
        $this->addColumn('comments', 'likes', Schema::TYPE_INTEGER . '  NOT NULL');
    }

    public function down()
    {
        $this->dropColumn('comments', 'likes', Schema::TYPE_INTEGER . '  NOT NULL');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
