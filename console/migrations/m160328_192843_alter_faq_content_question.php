<?php

use yii\db\Schema;
use yii\db\Migration;

class m160328_192843_alter_faq_content_question extends Migration
{
    public function up()
    {
        $this->alterColumn('faq_content', 'question', Schema::TYPE_TEXT. '  NOT NULL');
    }

    public function down()
    {
        echo "m160328_192843_alter_faq_content_question cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
