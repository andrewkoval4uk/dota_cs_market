<?php

use yii\db\Schema;
use yii\db\Migration;

class m160217_193150_create_menu_tables extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('menu', [
            'id' => $this->primaryKey(),
            'parent' => $this->string()->notNull()->defaultValue(0),
            'status' => $this->integer()->notNull()->defaultValue(0),
            'link' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('menu_titles', [
            'id' => $this->primaryKey(),
            'menu_id' => $this->integer()->notNull(),
            'lang' => $this->string()->notNull(),
            'title' => $this->string()->notNull()->defaultValue(''),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('menu');
        $this->dropTable('menu_titles');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
