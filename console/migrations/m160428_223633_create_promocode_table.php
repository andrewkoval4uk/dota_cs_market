<?php

use yii\db\Migration;

class m160428_223633_create_promocode_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('promocode_table', [
            'id' => $this->primaryKey(),
            'value' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'used_by' => $this->integer(),
            'used_at' => $this->integer(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('promocode_table');
    }
}
