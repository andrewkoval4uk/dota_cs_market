<?php

use yii\db\Schema;
use yii\db\Migration;

class m160227_103242_create_faq_entity_tables extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('faqs', [
            'id' => $this->primaryKey(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('faq_content', [
            'id' => $this->primaryKey(),
            'faq_id' => $this->integer()->notNull(),
            'lang' => $this->string()->notNull(),
            'question' => $this->string()->notNull()->defaultValue(''),
            'answer' => $this->string()->notNull()->defaultValue(''),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
        ], $tableOptions);


    }

    public function down()
    {
        $this->dropTable('faqs');
        $this->dropTable('faq_content');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
