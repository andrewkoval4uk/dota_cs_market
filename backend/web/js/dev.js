/**
 * Created by angelus on 28.02.2016.
 */
$( document ).ready(function(){
    $('#change_pwd').click(function(){
        var pwd =  $('#pwd_field');
        $( pwd ).toggleClass('closed');
        $( pwd ).find('input').val('');

    });
    $(".switcher-input").bootstrapSwitch();
    $('#addSteamIdInput').on('click', function(){
        $('#steambots').append('<input type="text" class="form-control" name="SteamBots[]" maxlength="255" required="required">');
    });
});