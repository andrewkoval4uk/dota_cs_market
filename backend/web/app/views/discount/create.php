<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DiscountPurchases */

$this->title = 'Create Discount Purchases';
$this->params['breadcrumbs'][] = ['label' => 'Discount Purchases', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-purchases-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
