<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Commisions */

$this->title = 'Update Commisions: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Commisions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="commisions-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
