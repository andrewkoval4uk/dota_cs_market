<?php

namespace backend\controllers;

use Yii;
use common\models\DiscountPurchases;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DiscountController implements the CRUD actions for DiscountPurchases model.
 */
class DiscountController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DiscountPurchases models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new DiscountPurchases();
        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            $model = new DiscountPurchases();
        }
        if(Yii::$app->request->post('hasEditable')) {
            $discountsId = Yii::$app->request->post('editableKey');
            $discount = DiscountPurchases::findOne($discountsId);
            $editIndex = Yii::$app->request->post('editableIndex');
            $out = Json::encode(['output'=>'', 'message' => '']);
            $post=[];
            $posted = Yii::$app->request->post('DiscountPurchases');
            if (Yii::$app->request->post('editableAttribute')=='amount') {
                $discount->amount = $posted[$editIndex]['amount'];
            }
            if (Yii::$app->request->post('editableAttribute')=='discount') {
                $discount->discount = $posted[$editIndex]['discount'];
            }
            if($discount->save()){

            }
            echo $out;
            return;
        }
        $dataProvider = new ActiveDataProvider([
            'query' => DiscountPurchases::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model'=>$model
        ]);
    }

    /**
     * Deletes an existing DiscountPurchases model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DiscountPurchases model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DiscountPurchases the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DiscountPurchases::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
