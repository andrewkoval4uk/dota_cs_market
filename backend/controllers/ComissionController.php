<?php

namespace backend\controllers;

use Yii;
use common\models\Commisions;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ComissionController implements the CRUD actions for Commisions model.
 */
class ComissionController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Commisions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Commisions();
        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            $model = new Commisions();
        }
//                VarDumper::dump(Yii::$app->request->post());die();
        if(Yii::$app->request->post('hasEditable')) {
            $discountsId = Yii::$app->request->post('editableKey');
            $editIndex = Yii::$app->request->post('editableIndex');
            $discount = Commisions::findOne($discountsId);
            $out = Json::encode(['output'=>'', 'message' => '']);
            $post=[];
            $posted = Yii::$app->request->post('Commisions');

            if (Yii::$app->request->post('editableAttribute')=='amount') {
                $discount->amount = $posted[$editIndex]['amount'];
            }
            if (Yii::$app->request->post('editableAttribute')=='overcharge') {
                $discount->overcharge = $posted[$editIndex]['overcharge'];
            }
            if($discount->save()){

            }
            echo $out;
            return;
        }

        $dataProvider = new ActiveDataProvider([
            'query' => Commisions::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model'=>$model
        ]);
    }

    /**
     * Deletes an existing Commisions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Commisions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Commisions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Commisions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
