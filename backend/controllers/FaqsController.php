<?php

namespace backend\controllers;

use common\models\FaqContent;
use Yii;
use common\models\Faqs;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FaqsController implements the CRUD actions for Faqs model.
 */
class FaqsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Faqs models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Faqs::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Faqs model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $faqs = array();
        $faqsLang = FaqContent::findAll([
            'faq_id' => $id,
        ]);

        foreach($faqsLang as $data){
            $faqs[$data->lang] = $data;
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'faqs' => $faqs
        ]);
    }

    /**
     * Creates a new Faqs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Faqs();
        $faqs = array();
        $langs = Yii::$app->params['langs'];
        $conts = Yii::$app->request->post('Contents');
        foreach($langs as $lang){
            $faqs[$lang] = [
                'status' => 0,
                'lang' => $lang,
                'question' => 'Вопрос '. $lang,
                'answer' => 'Ответ '.$lang,
            ];
        }
        $model->created_at = time();
        $model->updated_at = time();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(!is_null($conts)) {
                foreach ($conts as $lang => $data) {
                    $this->saveContent($lang, $data, $model->id);
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'faqs' => $faqs
            ]);
        }
    }

    /**
     * Updates an existing Faqs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $faqs = array();
        $langs = Yii::$app->params['langs'];
        $conts = Yii::$app->request->post('Contents');

        $faqsLang = FaqContent::findAll([
            'faq_id' => $id,
        ]);

        foreach($faqsLang as $data){
            $faqs[$data->lang] = [
                'status' => $data->status,
                'question' => $data->question,
                'answer' => $data->answer,
            ];
        }
        $model->updated_at = time();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if(!is_null($conts)) {
                foreach ($conts as $lang => $data) {
                    $this->saveContent($lang, $data, $model->id);
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'faqs' => $faqs
            ]);
        }
    }

    /**
     * Deletes an existing Faqs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if ($this->findModel($id)->delete()) {
            FaqContent::deleteAll('faq_id = :faq_id', [':faq_id' => $id]);
        }


        return $this->redirect(['index']);
    }


    public function saveContent($lang, $data, $id = 0) {

        $content = FaqContent::findOne([
            'faq_id' => $id,
            'lang' => $lang,
        ]);

        if(is_null($content)){
            $content = new FaqContent();
            $content->faq_id = $id;
            $content->lang = $lang;
        }

        $content->load($data);
        $content->status = $data['status'];
        $content->question = $data['question'];
        $content->answer = $data['answer'];
        $content->save();
        return $content;
    }
    /**
     * Finds the Faqs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Faqs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Faqs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
