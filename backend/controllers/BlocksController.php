<?php

namespace backend\controllers;

use common\models\BlocksLang;
use Yii;
use common\models\Blocks;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BlocksController implements the CRUD actions for Blocks model.
 */
class BlocksController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Blocks models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Blocks::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Blocks model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $blocks = array();
        $blocksLang = BlocksLang::findAll([
            'block_id' => $id,
        ]);

        foreach($blocksLang as $data){
            $blocks[$data->lang] = $data;
        }


        return $this->render('view', [
            'model' => $this->findModel($id),
            'blocks'=>$blocks
        ]);
    }

    /**
     * Creates a new Blocks model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Blocks();
        $blocks = array();
        $langs = Yii::$app->params['langs'];
        $conts = Yii::$app->request->post('Contents');
        foreach($langs as $lang){
            $blocks[$lang] = [
                'status' => 1,
                'lang' => $lang,
                'content' => 'content '.$lang,
            ];
        }
        $model->created_at = time();
        $model->updated_at = time();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if(!is_null($conts)) {
                foreach ($conts as $lang => $data) {
                    $this->saveContent($lang, $data, $model->id);
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'blocks'=>$blocks
            ]);
        }
    }
    public function saveContent($lang, $data, $id = 0){

        $content = BlocksLang::findOne([
            'block_id' => $id,
            'lang' => $lang,
        ]);

        if(is_null($content)){
            $content = new BlocksLang();
            $content->block_id = $id;
            $content->lang = $lang;
        }

        $content->load($data);
        $content->status = $data['status'];
        $content->content = $data['content'];
        $content->save();
        return $content;
    }
    /**
     * Updates an existing Blocks model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $blocks = array();
        $langs = Yii::$app->params['langs'];
        $conts = Yii::$app->request->post('Contents');

        $blocksLang = BlocksLang::findAll([
            'block_id' => $id,
        ]);

        foreach($blocksLang as $data){
            $blocks[$data->lang] = [
                'status' => $data->status,
                'content' => $data->content,
            ];
        }


        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if(!is_null($conts)) {
                foreach ($conts as $lang => $data) {
                    $this->saveContent($lang, $data, $model->id);
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'blocks'=>$blocks
            ]);
        }
    }

    /**
     * Deletes an existing Blocks model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Blocks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Blocks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Blocks::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
