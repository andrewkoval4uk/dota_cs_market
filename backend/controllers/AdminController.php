<?php

namespace backend\controllers;

use Yii;
use backend\models\Admin;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AdminController implements the CRUD actions for Admin model.
 */
class AdminController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Admin models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Admin::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Admin model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Admin model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Admin();
        $model->created_at = time();
        $model->status = Admin::STATUS_ACTIVE;
        $model->generateAuthKey();

        if(Yii::$app->request->post('Admin')) {
            $this->saveAdmin($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Admin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if(Yii::$app->request->post('Admin')) {
            $this->saveAdmin($model);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    protected function saveAdmin(Admin $model){
        $model->load(Yii::$app->request->post());
        $model->updated_at = time();
        $admin = Yii::$app->request->post('Admin');
        $model->username = $admin['username'];
        $model->email = $admin['email'];
        $model->pwd = $admin['pwd'];

        if($model->pwd != ''){
            $model->setPassword($model->pwd);
        }

        if ($model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return false;

    }

    /**
     * Deletes an existing Admin model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (count(Admin::find()->all()) == 0){
            \Yii::$app->session->setFlash('error', 'Cannot delete last admin');
        } else {
            $model = $this->findModel($id);
            if($model->role != Admin::ROLE_SUPER) {
                \Yii::$app->session->setFlash('success', $model->username . ' deleted');
                $model->delete();
            } else {
                \Yii::$app->session->setFlash('error', 'Cannot delete superadmin');
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Admin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Admin the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Admin::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
