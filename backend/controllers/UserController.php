<?php

namespace backend\controllers;

use common\models\Dcchats;
use Yii;
use common\models\User;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $banned = false;
        $request = Yii::$app->request->get();
//        $query = User::find()->where('steamID != :steamID', ['steamID'=>'']);
        $condition = 'steamID != :steamID';
        $params = ['steamID' => ''];
        if(isset($request['status']) && isset($request['chat'])) {
            $condition .= ' AND status=:status OR chat=:chat';
            $params[':status'] = User::STATUS_BANNED;
            $params[':chat'] = 0;
            $banned = true;
        }

        $dataProvider = new ActiveDataProvider([
            'query' => User::find()->where($condition, $params)
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'banned' => $banned,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        die('Not Available');
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionBanned() {
        $request = Yii::$app->request->get();
        if(isset($request['user'])) {
        $user = User::find()->where(['id' => $request['user']])->one();
        if(isset($request['chat'])) {
         if($request['chat']==User::STATUS_BANNED) {
             $user->chat = User::STATUS_NOT_BANNED;
         } elseif($request['chat']==User::STATUS_NOT_BANNED){
             $user->chat = User::STATUS_BANNED;
         }

        } elseif(isset($request['user_status'])) {
            if($request['user_status']==User::STATUS_ACTIVE) {
                $user->status = User::STATUS_DELETED;
            } elseif($request['user_status']==User::STATUS_DELETED){
                $user->status = User::STATUS_ACTIVE;
            }
        }
            if($user->save()) {
                return $this->render('view', [
                    'model' => $user,
                ]);
            }
        }

    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $user = User::findOne($id);
        $messages = Dcchats::find()->where(['sender'=>$user->steamId])->all();
        if($messages) {
            foreach($messages as $m) {
                $m->delete();
            }

        }
        $user->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
