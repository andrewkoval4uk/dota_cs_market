<?php

namespace backend\controllers;

use common\models\MenuTitles;
use Yii;
use common\models\Menu;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Menu::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Menu model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $titles = array();
        $titlesModels = MenuTitles::findAll([
            'menu_id' => $id,
        ]);

        foreach($titlesModels as $data){
            $titles[$data->lang] = $data;
        }

        $attributes = [
            [
                'label' => 'Title',
                'value' => $model->getTitle(),
            ],
        ];

        foreach($titles as $lang => $data){
            $attributes[] = [
                'label' => 'Title (' . $data->lang . ')',
                'value' => $data->title,
            ];
        }
        $attributes = array_merge($attributes, [

            [
                'label' => \Yii::t('app', 'Parent ITem'),
                'value' => $model->getParent(),
            ],
            [
                'label' => \Yii::t('app', 'Status'),
                'value' => $model->status == 0 ? 'disabled' : 'enabled',
            ],
            'link',
            [
                'label' => \Yii::t('app', 'Created'),
                'value' => date("F j, Y, g:i a", $model->created_at),
            ],
            [
                'label' => \Yii::t('app', 'Updated'),
                'value' => date("F j, Y, g:i a", $model->updated_at),
            ],
        ]);
        return $this->render('view', [
            'model' => $model,
            'titles' => $titles,
            'attributes' => $attributes,
        ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Menu();
        $model->created_at = time();
        $model->updated_at = time();
        $langs = Yii::$app->params['langs'];
        $parents = [0 => 'no parent'];
        $items = Menu::find()->all();
        $titles = [];

        foreach($langs as $lang){
            $titles[$lang] = [
                'status' => 1,
                'title' => $lang . ' title',
            ];
        }

        foreach ($items as $item){
            $parents[$item->id] = $item->getTitle();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $items = Yii::$app->request->post('MenuTitle');
            if(!is_null($items)) {
                foreach ($items as $lang => $data) {
                    $this->saveTitle($lang, $data, $model->id);
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'langs' => $langs,
                'parents' => $parents,
                'titles' => $titles,
            ]);
        }
    }

    public function saveTitle($lang, $data, $id = 0){

        $content = MenuTitles::findOne([
            'menu_id' => $id,
            'lang' => $lang,
        ]);

        if(is_null($content)){
            $content = new MenuTitles();
            $content->created_at = time();
            $content->menu_id = $id;
            $content->lang = $lang;
        }

        $content->load($data);
        $content->updated_at = time();
        $content->status = 1;
        $content->title = $data['title'];
        $content->save();
        return $content;
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->updated_at = time();
        $langs = Yii::$app->params['langs'];
        $parents = [0 => 'no parent'];
        $items = Menu::find()->where('id != :id', ['id' => $id])->all();;
        $titles = [];

        $titleModels = MenuTitles::findAll([
            'menu_id' => $id,
        ]);

        foreach($titleModels as $data){
            $titles[$data->lang] = [
                'status' => $data->status,
                'title' => $data->title,
            ];
        }

        foreach ($items as $item){
            $parents[$item->id] = $item->getTitle();
        }
//VarDumper::dump($titles); die;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $items = Yii::$app->request->post('MenuTitle');
            if(!is_null($items)) {
                foreach ($items as $lang => $data) {
                    $this->saveTitle($lang, $data, $model->id);
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'langs' => $langs,
                'parents' => $parents,
                'titles' => $titles,
            ]);
        }
    }

    /**
     * Deletes an existing Menu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        MenuTitles::deleteAll('menu_id = :menu_id', [':menu_id' => $id]);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
