<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Bots */

$this->title = 'Изменить бота: ' . $model->steam_id;
$this->params['breadcrumbs'][] = ['label' => 'Bots', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->steam_id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $model->steam_id;
?>
<div class="bots-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
