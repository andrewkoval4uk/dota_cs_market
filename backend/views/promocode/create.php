<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PromocodeTable */

$this->title = 'Создать Промокод';
$this->params['breadcrumbs'][] = ['label' => 'Промокод', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promocode-table-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
