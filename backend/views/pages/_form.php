<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Pages */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
$this->registerJs(
    '
      CKEDITOR.replace( "editor_ua" );'
);
$this->registerJs(
    '
      CKEDITOR.replace( "editor_ru" );'
);
$this->registerJs(
    '
      CKEDITOR.replace( "editor_en" );'
);
?>
<div class="pages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meta_desc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(\common\models\Tags::getStatus()); ?>

    <div class="nav-bars">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <?php foreach ($pages as $lang => $data) : ?>
                <li role="presentation" class="<?php echo isset($b) ? '' : 'active'; $b = true; ?>">
                    <a href="#<?= $lang; ?>" aria-controls="<?= $lang; ?>" role="tab" data-toggle="tab"><?= \common\models\News::getLangName($lang); ?></a>
                </li>
            <?php endforeach; ?>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <?php foreach ($pages as $lang => $data) : ?>
                <div role="tabpanel" class="tab-pane <?php echo isset($a) ? '' : 'active'; $a = true; ?>" id="<?= $lang; ?>">
                    <?= Html::label('Включить перевод', "Contents[$lang][status]", ['class'=>'control-label'])?>
                    <?= Html::dropDownList("Contents[$lang][status]", $data['status'], \common\models\Tags::getStatus(), ['class'=>'form-control'])?>

                    <?= Html::label('Наименование страницы', "Contents[$lang][name]", ['class'=>'control-label'])?>
                    <?= Html::textInput("Contents[$lang][name]", $data['name'], ['class'=>'form-control'])?>

                    <?= Html::label('Содержимое страницы', "Contents[$lang][content]", ['class'=>'control-label'])?>
                    <?= Html::textarea("Contents[$lang][content]", $data['content'],
                        ['class'=>'form-control news-text-redactor', 'id'=>'editor_'.$lang])?>

                </div>
            <?php endforeach; ?>
        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>