<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Pages */

$this->title = $model->meta_title;
$this->params['breadcrumbs'][] = ['label' => 'Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="nav-bars">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <?php foreach ( $pages as $lang => $data) : ?>
                <li role="presentation" class="<?php echo isset($b) ? '' : 'active'; $b = true; ?>">
                    <a href="#<?= $lang; ?>" aria-controls="<?= $lang; ?>" role="tab" data-toggle="tab"><?= \common\models\News::getLangName($lang); ?></a>
                </li>
            <?php endforeach; ?>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <?php foreach ($pages as $lang => $data) : ?>
                <div role="tabpanel" class="tab-pane <?php echo isset($a) ? '' : 'active'; $a = true; ?>" id="<?= $lang; ?>">
                    <?= DetailView::widget([
                        'model' => $data,
                        'attributes' => [

                            [
                                'label' => \Yii::t('app', 'Language'),
                                'value' => \common\models\News::getLangName($data->lang),
                            ],
                            [
                                'label' => \Yii::t('app', 'Включить перевод'),
                                'value' => $data->status == 1 ? 'Включен' : 'Отключен',
                            ],
                        ],
                    ]) ?>
                    <p class="lead">Заголовок страницы</p>
                    <div class="well" style="background-color: white">
                        <?= $data->name; ?>
                    </div>
                    <p class="lead">Содержимое страницы</p>
                    <div class="well" style="background-color: white">
                        <?= $data->content; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'page_name',
            [
                'attribute' => 'Состояние страницы на сайте',
                'value'=>$model->status == 1 ? 'Включена' : 'Отключена'
            ],
            'meta_title',
            'meta_desc',
            'alias',
            [
                'attribute' => 'Создан',
                'value'=>date('Y-m-d H:i:s',$model->created_at)
            ],
            [
                'attribute' => 'Обновлен',
                'value'=>date('Y-m-d H:i:s',$model->updated_at)
            ],
        ],
    ]) ?>

</div>
