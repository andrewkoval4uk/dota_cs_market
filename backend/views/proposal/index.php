<?php

use yii\widgets\Pjax;
use yii\helpers\Html;
use kartik\grid\GridView;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки на вывод средств';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proposal-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'pjax' => true,
        'export' => false,

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'user.username',
            'user.balance',
            'amount',
            [
                'attribute' => 'payment',
                'format' => 'text', // Возможные варианты: raw, html
                'content' =>
                    function($data){
                        return \common\models\Proposal::getPayment($data->payment);
                    },
            ],
            'account',
            [
                'attribute' => 'status',
                'format' => 'text', // Возможные варианты: raw, html
                'content' =>
                    function($data){
                        return \common\models\Proposal::getStatus($data->status);
                    },
            ],
            [
                'attribute'=>'date',
                'label'=>'Дата',
                'format'=>'text', // Возможные варианты: raw, html
                'content'=>
                    function($data){
                        return date('Y-m-d H:i:s', $data->date);
                    },
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{accept}{delete}',
                'buttons'=>[
                    'accept' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-plus"></span>', $url, [
                            'title' => Yii::t('yii', 'Create'),
                            'data-confirm' => "Вы уверены, что хотите пометить эту заявку как принятую?",
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('yii', 'Create'),
                            'data-confirm' => "Вы уверены, что хотите пометить эту заявку как отклоненную?",
                            'aria-label' => "Delete",
                            'data-method' => "post",
                            'data-pjax' => "0",
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>

</div>
