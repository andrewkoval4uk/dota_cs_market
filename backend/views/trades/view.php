<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Trades;

/* @var $this yii\web\View */
/* @var $model common\models\Trades */

$this->title = \yii::t('app', 'Trade #') . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Trades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trades-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'Seller',
                'value'=> $model->seller->username,
            ],
            [
                'attribute' => 'Buyer',
                'value'=> $model->buyer->username,
            ],
            'item_name',
            'amount',
            [
                'attribute' => 'Seller',
                'value'=> Trades::getStatus($model->status),
            ],
            [
                'attribute' => 'Date',
                'value'=>date('Y-m-d H:i:s',$model->date),
            ],
        ],
    ]) ?>

</div>
