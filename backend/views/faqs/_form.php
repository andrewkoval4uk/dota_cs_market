<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Faqs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faqs-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="nav-bars">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <?php foreach ($faqs as $lang => $data) : ?>
                <li role="presentation" class="<?php echo isset($b) ? '' : 'active'; $b = true; ?>">
                    <a href="#<?= $lang; ?>" aria-controls="<?= $lang; ?>" role="tab" data-toggle="tab"><?= \common\models\News::getLangName($lang); ?></a>
                </li>
            <?php endforeach; ?>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <?php foreach ($faqs as $lang => $data) : ?>
                <div role="tabpanel" class="tab-pane <?php echo isset($a) ? '' : 'active'; $a = true; ?>" id="<?= $lang; ?>">
                    <?= Html::label('Включить перевод', "Contents[$lang][status]", ['class'=>'control-label'])?>
                    <?= Html::dropDownList("Contents[$lang][status]", $data['status'], \common\models\Tags::getStatus(), ['class'=>'form-control'])?>

                    <?= Html::label('Вопрос', "Contents[$lang][question]", ['class'=>'control-label'])?>
                    <?= Html::textInput("Contents[$lang][question]", $data['question'], ['class'=>'form-control'])?>

                    <?= Html::label('Ответ', "Contents[$lang][answer]", ['class'=>'control-label'])?>
                    <?= Html::textarea("Contents[$lang][answer]", $data['answer'],
                        ['class'=>'form-control news-text-redactor', 'id'=>'editor'])?>

                </div>
            <?php endforeach; ?>
        </div>


    <?= $form->field($model, 'status')->dropDownList(\common\models\Tags::getStatus()); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
