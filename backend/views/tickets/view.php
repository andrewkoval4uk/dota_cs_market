<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Tickets */

$this->title = $model->theme;
$this->params['breadcrumbs'][] = ['label' => 'Тикеты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tickets-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
//            'theme',
            [
                'label' => \yii::t('app', 'Тема'),
                'value' => $model->getThemeName(),
            ],
            'text:ntext',
            [
                'label' => \Yii::t('app', 'Состояние тикета'),
                'value' => \common\models\Tickets::getStatus($model->status),
            ],
            [
                'attribute' => 'Создан',
                'value'=>date('Y-m-d H:i:s',$model->created_at)
            ],
            [
                'attribute' => 'Обновлен',
                'value'=>date('Y-m-d H:i:s',$model->updated_at)
            ],
        ],
    ]);
    ?>
<?php
if(!is_null($ticketMessages)){
  foreach ($ticketMessages as $mess) { ?>
      <div class="well" style="background-color: white">
          <p><small><?= 'От ' . \common\models\Messages::getSender($mess->sender)?></small></p>
          <?= $mess->text; ?>
          <p><small><?= date('Y-m-d H:i:s',$mess->date)?></small></p>
      </div>
  <?php
  }
}
?>
<?php
if($model->status==1) { ?>
    <div class="create_message">
        <?php $form = ActiveForm::begin(); ?>
        <p class="lead">Создать новое сообщение</p>
        <?= Html::label('Текст сообщения', "Message[text]", ['class'=>'control-label'])?>
        <?= Html::textarea("Message[text]", '', ['class'=>'form-control news-text-redactor'])?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Создать'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

<?php } ?>
</div>
