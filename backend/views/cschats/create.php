<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Cschats */

$this->title = 'Create Cschats';
$this->params['breadcrumbs'][] = ['label' => 'Cschats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cschats-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
