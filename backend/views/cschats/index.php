<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $msg common\models\Cschats */
/* @var $chat array of common\models\Cschats */

$this->title = 'Cs Chat';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cschats-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if(!$banned): ?>
        <?= Html::a('Штрафы', ['index?status=' . \common\models\Dcchats::STATUS_REPORT], ['class' => 'btn btn-danger']);?>
    <?php else: ?>
        <?= Html::a('Все пользователи', ['index'], ['class' => 'btn btn-danger']);?>
    <?php endif; ?>

    <?php if(count($chat) == 0): ?>
        <p><?= \yii::t('app', 'There are no messages in this chat yet...'); ?></p>
    <?php else: ?>
        <?php foreach($chat as $msg): ?>
            <p><?= '( ' . date('Y-m-d H:i:s', $msg->date) . ' ) ' . $msg->nickname . ': ' . $msg->message; ?></p>
            <?php if($banned) : ?>
                <?= Html::a('Удалить', ['index', 'status'=>2, 'message'=>$msg->id], ['class' => 'btn btn-danger']);?>
                <?= Html::a('Разрешить', ['index', 'status'=>\common\models\Dcchats::STATUS_NORM , 'message'=>$msg->id], ['class' => 'btn btn-success']); ?>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
