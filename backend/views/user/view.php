<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], [
            'class' => 'btn btn-success',
        ]) ?>
    </p>

    <div id="users-detail-vew-container" >
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'username',
                [
                    'attribute' => 'status',
                    'format'=>'raw',
                    'value' => $model->status ? Html::a('Запретить продажу', ['banned', 'user_status' => $model->status, 'user'=>$model->id], ['class' => 'btn btn-danger'])
                            :
                            Html::a('Разрешить продажу', ['banned', 'user_status' => $model->status, 'user'=>$model->id], ['class' => 'btn btn-primary'])
                ],
                [
                    'attribute' => 'steamId',
                    'format'=>'raw',
                    'value' => '<a target="_blank" href="http://steamcommunity.com/profiles/' . $model->steamId . '" >' . $model->steamId . '</a>'
                ],
                [
                    'attribute' => 'balance',
                    'format'=>'raw',
                    'value' => number_format($model->balance, 0, ',', ' ') . ' P',
                ],
                'discount',
                [
                    'attribute' => 'Создана',
                    'value'=>date('Y-m-d H:i:s', $model->created_at)
                ],
                [
                    'attribute' => 'Обновлена',
                    'value'=>date('Y-m-d H:i:s', $model->updated_at)
                ],
                [
                    'attribute' => 'chat',
                    'format'=>'raw',
                    'value' => $model->chat ? Html::a('Забанить', ['banned', 'chat' => $model->chat, 'user'=>$model->id], ['class' => 'btn btn-danger'])
                            :
                            Html::a('Снять штраф', ['banned', 'chat' => $model->chat, 'user'=>$model->id], ['class' => 'btn btn-primary'])
                ],
            ],
        ]) ?>
    </div>
</div>
