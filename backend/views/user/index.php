<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $banned boolean */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if(!$banned): ?>
        <?= Html::a('Забаненные пользователи', ['index?status=' . \common\models\User::STATUS_BANNED . '&chat=0'], ['class' => 'btn btn-danger']);?>
    <?php else: ?>
        <?= Html::a('Все пользователи', ['index'], ['class' => 'btn btn-danger']);?>
    <?php endif; ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'username',
            [
                'attribute'=>'status',
                'content'=>
                    function($data){
                        return \common\models\User::getStatus($data->status);
                    },
            ],
            [
                'attribute'=>'email',
                'content'=>
                    function($data){
                        return $data->checkEmail();
                    },
            ],
            [
                'attribute'=>'steamId',
                'format'=>'raw',
                'content'=>
                    function($data){
                        return '<a target="_blank" href="http://steamcommunity.com/profiles/' . $data->steamId . '" >' . $data->steamId . '</a>';
                    },
            ],
            [
                'attribute'=>'balance',
                'format'=>'raw',
                'content'=>
                    function($data){
                        return number_format($data->balance, 0, ',', ' ') . ' P';
                    },
            ],
            'discount',
            [
                'attribute'=>'chat',
                'content'=>
                    function($data){
                        return \common\models\User::getStatusChat($data->chat);
                    },
            ],
            [
                'attribute'=>'created_at',
                'label'=>'Дата создания',
                'format'=>'text',
                'content'=>
                    function($data){
                        return date('Y-m-d H:i:s', $data->created_at);
                    },
            ],
            [
                'attribute'=>'updated_at',
                'label'=>'Дата редактирования',
                'format'=>'text',
                'content'=>
                    function($data){
                        return date('Y-m-d H:i:s', $data->updated_at);
                    },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
