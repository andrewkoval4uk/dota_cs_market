<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Menu */
/* @var $form yii\widgets\ActiveForm */
/* @var $parents array */
/* @var $langs array */
?>

<div class="menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php foreach($titles as $lang => $data): ?>
        <?= Html::label('Title (' . $lang . ')', "MenuTitle[$lang][title]", ['class'=>'control-label'])?>
        <?= Html::textInput("MenuTitle[$lang][title]", $data['title'], ['class'=>'form-control'])?>
    <?php endforeach; ?>

    <?= $form->field($model, 'parent')->dropDownList($parents); ?>

    <?= $form->field($model, 'status')->dropDownList(\common\models\Tags::getStatus()); ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
