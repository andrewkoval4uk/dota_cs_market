<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Admin;

/* @var $this yii\web\View */
/* @var $model backend\models\Admin */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php if($model->isNewRecord): ?>
        <?= $form->field($model, 'pwd')->textInput(['maxlength' => true])->label('Password'); ?>
    <?php else: ?>
    <span id="change_pwd">
        <?= \yii::t('app', 'Change password'); ?>
    </span>
    <div id="pwd_field" class="closed">
        <?= $form->field($model, 'pwd')->textInput(['maxlength' => true])->label('New Password'); ?>
    </div>
    <?php endif; ?>

    <?= $form->field($model, 'role')->dropDownList(Admin::getRole()); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
