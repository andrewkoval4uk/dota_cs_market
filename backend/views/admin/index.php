<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Admin;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Admins';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Admin', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'username',
             'email:email',
            [
                'attribute' => 'role',
                'label' => 'Role',
                'format' => 'text', // ��������� ��������: raw, html
                'content' =>
                    function($data){
                        return ADmin::getRole($data->role);
                    },
            ],
            [
                'attribute' => 'status',
                'label' => 'Status',
                'format' => 'text', // ��������� ��������: raw, html
                'content' =>
                    function($data){
                        return ADmin::getStatus($data->status);
                    },
            ],
            [
                'attribute' => 'created_at',
                'label' => 'Created At',
                'format' => 'text', // ��������� ��������: raw, html
                'content' =>
                    function($data){
                        return date('Y-m-d H:i:s', $data->created_at);
                    },
            ],
            [
                'attribute' => 'updated_at',
                'label' => 'Updated At',
                'format' => 'text', // ��������� ��������: raw, html
                'content' =>
                    function($data){
                        return date('Y-m-d H:i:s', $data->updated_at);
                    },
            ],

            ['class' => 'yii\grid\ActionColumn', 'buttons'=>[
                'remove' => function ($model) {
                        $urlConfig = [];
                        foreach ($model->primaryKey() as $pk) {
                            $urlConfig[$pk] = $model->$pk;
                            $urlConfig['type'] = $model->type;
                        }
                        $url = \yii\helpers\Url::toRoute(array_merge(['delete'], $urlConfig));
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => \Yii::t('yii', 'Delete'),
                            'data-confirm' => \Yii::t('yii', 'Are you sure to delete this item?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ]);
                    }
            ]],
        ],
    ]); ?>

</div>
