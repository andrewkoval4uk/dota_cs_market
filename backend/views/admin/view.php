<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \backend\models\Admin;

/* @var $this yii\web\View */
/* @var $model backend\models\Admin */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Admins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'username',
            'email:email',
            [
                'attribute' => 'status',
                'value' => Admin::getStatus($model->status),
            ],
            [
                'attribute' => 'role',
                'value' => Admin::getRole($model->role),
            ],
            [
                'attribute' => 'Created at',
                'value' => date('Y-m-d H:i:s',$model->created_at)
            ],
            [
                'attribute' => 'Updated_at',
                'value' => date('Y-m-d H:i:s',$model->updated_at)
            ],
        ],
    ]) ?>

</div>
