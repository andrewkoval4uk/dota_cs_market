<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Blocks */

$this->title = 'Create Blocks';
$this->params['breadcrumbs'][] = ['label' => 'Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->block_uniq;
?>
<div class="blocks-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'blocks'=>$blocks
    ]) ?>

</div>
