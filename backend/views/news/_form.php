<?php

use yii\helpers\Html;
use dosamigos\fileupload\FileUpload;
use yii\widgets\ActiveForm;
use dosamigos\multiselect\MultiSelect;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
$this->registerJs(
    '
      CKEDITOR.replace( "editor_ru" );'
);

$this->registerJs(
    '
      CKEDITOR.replace( "editor_ua" );'
);

$this->registerJs(
    '
      CKEDITOR.replace( "editor_en" );'
);
?>
<div class="news-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <hr />

    <div class="nav-bars">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <?php foreach ($news as $lang => $data) : ?>
                <li role="presentation" class="<?php echo isset($b) ? '' : 'active'; $b = true; ?>">
                    <a href="#<?= $lang; ?>" aria-controls="<?= $lang; ?>" role="tab" data-toggle="tab"><?= \common\models\News::getLangName($lang); ?></a>
                </li>
            <?php endforeach; ?>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <?php foreach ($news as $lang => $data) : ?>
                <div role="tabpanel" class="tab-pane <?php echo isset($a) ? '' : 'active'; $a = true; ?>" id="<?= $lang; ?>">
                    <?= Html::label('Статус языка', "Contents[$lang][status]", ['class'=>'control-label'])?>
                    <?= Html::dropDownList("Contents[$lang][status]", $data['status'], ['Язык включен', 'Язык выключен'], ['class'=>'form-control'])?>

                    <?= Html::label('Title', "Contents[$lang][title]", ['class'=>'control-label'])?>
                    <?= Html::textInput("Contents[$lang][title]", $data['title'], ['class'=>'form-control'])?>

                    <?= Html::label('Текст новости', "Contents[$lang][content]", ['class'=>'control-label'])?>
                    <?= Html::textarea("Contents[$lang][content]", $data['content'], ['class'=>'form-control news-text-redactor', 'id'=>'editor_'.$lang])?>

                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <?= Html::label('Выберите теги для новости', "tags[label]", ['class'=>'control-label'])?>

    <div class="clearfix"></div>

    <?php
         if(!empty($tags) && isset($tags_for_news)) {
             echo MultiSelect::widget([
                 "options" => ['multiple'=>"multiple"],
                 'data'    => $tags,
                 'value'   => $tags_for_news,
                 'name'    => 'Tags'
            ]);
        }
    ?>

    <div class="clearfix"></div>

    <?= Html::label('Выберите рубрики для новости', "rubrics[label]", ['class'=>'control-label'])?>

    <div class="clearfix"></div>

    <?php
        if(!empty($rubrics) && isset($rubrics_for_news)) {
            echo MultiSelect::widget([
                'options' => ['multiple'=>'multiple'],
                'data'    => $rubrics,
                'value'   => $rubrics_for_news,
                'name'    => 'Rubrics'
            ]);
        }
    ?>

    <?= $form->field($model, 'status')->dropDownList(\common\models\Tags::getStatus()); ?>

    <?php if ($model->image_path!==NULL): ?>
        <div class="">
            <img style="width:40%" src="<?= $model->getImage(); ?>">
        </div>
    <?php endif; ?>

    <?=$form->field($upload, 'imageFile')->fileInput() ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>