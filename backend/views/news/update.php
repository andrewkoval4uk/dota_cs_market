<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = 'Update News: ' . ' ' . $news['ru']['title'];
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="news-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'             => $model,
        'upload'            => $upload,
        'news'              => $news,
        'tags'              => $tags,
        'rubrics'           => $rubrics,
        'rubrics_for_news'  => $rubrics_for_news,
        'tags_for_news'     => $tags_for_news
    ]) ?>

</div>
