Yii 2 Advanced Project Template
===============================

Yii 2 Advanced Project Template is a skeleton [Yii 2](http://www.yiiframework.com/) application best for
developing complex Web applications with multiple tiers.

The template includes three tiers: front end, back end, and console, each of which
is a separate Yii application.

The template is designed to work in a team development environment. It supports
deploying the application in different environments.

Documentation is at [docs/guide/README.md](docs/guide/README.md).

[![Latest Stable Version](https://poser.pugx.org/yiisoft/yii2-app-advanced/v/stable.png)](https://packagist.org/packages/yiisoft/yii2-app-advanced)
[![Total Downloads](https://poser.pugx.org/yiisoft/yii2-app-advanced/downloads.png)](https://packagist.org/packages/yiisoft/yii2-app-advanced)
[![Build Status](https://travis-ci.org/yiisoft/yii2-app-advanced.svg?branch=master)](https://travis-ci.org/yiisoft/yii2-app-advanced)

DIRECTORY STRUCTURE
-------------------

```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
tests                    contains various tests for the advanced application
    codeception/         contains tests developed with Codeception PHP Testing Framework
```


Attention:
1) Dota2 is the main section of the site.
To add new site section <name> with custom layout:
a) Create all necessary controllers like:
class Name<DotaControllerName> extends <DotaControllerName> implements ViewContextInterface
{

    public function beforeAction($action) {
        $this->view->params['layout'] = '<name>';
        Yii::$app->session->set('layout', '<name>');
        return parent::beforeAction($action);
    }

    public function getViewPath()
    {
        return Yii::getAlias('@frontend/views/<name>');
    }

}
b) new markup implements like <body id="<name>">  in your layouts/main.php. Style your site with it
c) Also you can use
$this->layout = 'your-custom-layout'
instead of
$this->view->params['layout'] = '<name>';
Yii::$app->session->set('layout', '<name>');

2) To get current user you should use User::getCurrentUser()
Yii::$app->user->getIdentity()->profile contains Steam user info
Yii::$app->user->getIdentity()->profile['name'] - steamID