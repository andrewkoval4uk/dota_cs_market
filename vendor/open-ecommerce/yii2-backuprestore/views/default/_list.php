<?php

use yii\helpers\Html;

//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\Url;


echo GridView::widget([
    'id' => 'install-grid',
    'export' => false,
    'dataProvider' => $dataProvider,
    'resizableColumns' => false,
    'showPageSummary' => false,
    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
    'responsive' => true,
    'hover' => true,
    'panel' => [
        'heading' => '<h3 class="panel-title"> Database backup files</h3>',
        'type' => 'primary',
        'showFooter' => false
    ],
    // set your toolbar
    'toolbar' => [
        ['content' =>
            Html::a('<i class="glyphicon glyphicon-plus"></i>  Create Backup', ['create'], ['class' => 'btn btn-success create-backup']). ' ',
        ],
    ],
    'columns' => array(
        'name',
        'size:size',
        'create_time',
        'modified_time:relativeTime',
//        [
//            'attribute' => 'name',
//            'label' => 'Download',
//            'format' => 'text',
//            'content' =>
//                function($model){
//                    $url = Yii::$app->basePath . '/_backup/' . $model['name'];
//
//                    return Html::a('<span class="glyphicon glyphicon-download-alt"></span>', $url, [
//                        'title' => Yii::t('app', 'Download'),
//                        'download' => "dump.sql",
//                    ]);
//                },
//        ],
    ),
]);
?>