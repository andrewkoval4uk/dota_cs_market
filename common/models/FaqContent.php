<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "faq_content".
 *
 * @property integer $id
 * @property integer $faq_id
 * @property string $lang
 * @property string $question
 * @property string $answer
 * @property integer $status
 */
class FaqContent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq_content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['faq_id', 'lang'], 'required'],
            [['faq_id', 'status'], 'integer'],
            [['lang', 'question', 'answer'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'faq_id' => 'Faq ID',
            'lang' => 'Lang',
            'question' => 'Question',
            'answer' => 'Answer',
            'status' => 'Status',
        ];
    }
    static function getQuestion($id) {
        $faqsLang = self::find()->where(['faq_id' => $id, 'lang' => Yii::$app->params['deflang']])->one();
        return $faqsLang;
    }
}
