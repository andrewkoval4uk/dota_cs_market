<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "notifications".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $status
 * @property string $text
 * @property integer $created
 */
class Notifications extends \yii\db\ActiveRecord
{

    const STATUS_NEW = 0;
    const STATUS_READ = 1;

    public static function getStatus($status=false){

        $statuses = [
            self::STATUS_NEW => 'new',
            self::STATUS_READ => 'read',
        ];

        if(false === $status){
            return $statuses;
        } else {
            return isset($statuses[$status]) ? $statuses[$status] : $status;
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notifications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'created'], 'required'],
            [['user_id', 'status', 'created'], 'integer'],
            [['text'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'status' => 'Status',
            'text' => 'Text',
            'created' => 'Created',
        ];
    }

    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function getAllUserNotifications($id){
        return self::findAll(['user_id' => $id, 'status' => self::STATUS_NEW]);
    }

}
