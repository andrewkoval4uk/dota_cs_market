<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "comments_likes_state".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $comment_id
 * @property integer $positive
 * @property integer $negative
 */
class CommentsLikesState extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments_likes_state';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'comment_id', 'positive', 'negative'], 'required'],
            [['user_id', 'comment_id', 'positive', 'negative'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'comment_id' => 'Comment ID',
            'positive' => 'Positive',
            'negative' => 'Negative',
        ];
    }
}
