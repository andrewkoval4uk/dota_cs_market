<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "news_lang".
 *
 * @property integer $id
 * @property integer $new_id
 * @property string $lang
 * @property string $title
 * @property string $content
 * @property integer $status
 */
class NewsLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['new_id', 'status'], 'integer'],
            [['content'], 'string'],
            [['lang', 'title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'new_id' => 'New ID',
            'lang' => 'Lang',
            'title' => 'Title',
            'content' => 'Content',
            'status' => 'Status',
        ];
    }
}
