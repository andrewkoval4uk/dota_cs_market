<?php

namespace common\models;

use Yii;
use yii\base\Theme;

/**
 * This is the model class for table "tickets".
 *
 * @property integer $id
 * @property string $theme
 * @property string $text
 * @property integer $status
 * @property integer $theme_id
 * @property string $other_theme
 * @property integer $created_at
 * @property integer $updated_at
 */
class Tickets extends \yii\db\ActiveRecord
{
    public $checkbox;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tickets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['theme', 'text'], 'required'],
            [['text'], 'string'],
            [['status', 'user_id', 'created_at', 'updated_at'], 'integer'],
            [['theme'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'theme' => 'Тема',
            'text' => 'Текст',
            'status' => 'Статус тикета',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
        ];
    }
    static function getStatus($status) {
        if($status==1){
            return 'Открыт';
        }
         else {
             return  'Решен';
         }

    }

    public function getThemeName(){
        if($this->theme_id === 0){
            return $this->other_theme;
        } else {
            return $this->newTheme->name;
        }
    }

    public function getNewTheme(){
        return $this->hasOne(Themes::className(), ['id' => 'theme_id']);
    }

    public function getMessages(){
        return $this->hasMany(Messages::className(), ['ticket_id' => 'id']);
    }

    public static function cutText($string){
        if(strlen($string) > 100){
            $string = substr($string, 0, 100);
            $string = rtrim($string, "!,.-");
//            $string = substr($string, 0, strrpos($string, ' '));
            $string .= "… ";
        }
        return $string;
    }
}
