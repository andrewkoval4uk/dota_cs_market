<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tags".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 */
class Tags extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }

    static function getStatus() {
        return $statuses = array(
            0 => 'Не показывать',
            1 => 'Показывать',
        );
    }
    public static function getAllTags(){
        $tags = self::findAll(['status' => '1']);
        $nameArr = array();
        foreach($tags as $tag) {
            $nameArr[$tag->id]=$tag->name;
        }
        return $nameArr;
    }

    public static function getAllTagsNews($tags){
        $rubIds = array();
        foreach($tags as $r) {
            $rubIds[]=$r->id;
        }
        return $rubIds;
    }

    public function getNews(){
        return $this->hasMany(News::className(), ['id' => 'new_id'])->viaTable('tags_news', ['tag_id' => 'id']);
    }

    public static function getNewsByTag($id = false) {
        return self::find()
            ->where(['status' => News::STATUS_ACTIVE])
            ->with('news')
            ->asArray()
            ->all();
    }

    static function getTagsByNews($id) {
        return self::find()
            ->where(['id' => $id])
            ->with('news')
            ->asArray()
            ->all();
    }
}
