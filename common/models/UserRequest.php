<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_request".
 *
 * @property integer $id
 * @property integer $id_request
 * @property integer $user_id
 * @property integer $type
 * @property integer $amount
 */
class UserRequest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_request', 'user_id', 'type', 'amount'], 'required'],
            [['id_request', 'user_id', 'type', 'amount'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_request' => 'Id Request',
            'user_id' => 'User ID',
            'type' => 'Type',
            'amount' => 'Amount',
        ];
    }
    static function getType($type){
        if ($type==2){
            return 'Уведомление';
        } else {
            return 'Автопокупка';
        }
    }
}
