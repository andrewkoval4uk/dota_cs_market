<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "discount_purchases".
 *
 * @property integer $id
 * @property integer $amount
 * @property integer $discount
 */
class DiscountPurchases extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discount_purchases';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount', 'discount'], 'required'],
            [['amount', 'discount'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'amount' => 'Общая сумма',
            'discount' => 'Скидка',
        ];
    }
}
