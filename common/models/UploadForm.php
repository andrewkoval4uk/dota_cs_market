<?php
namespace common\models;

use yii\base\Model;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;
use Yii;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;
    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif, jpeg', 'checkExtensionByMimeType' => false],
        ];
    }
    public function upload()
    {
        $path= Yii::getAlias('@frontend') . '/web/uploads';
        if(!empty($this->imageFile)) {

            if ($this->validate()) {
                $this->imageFile->saveAs($path . '/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
                return true;
            } else {
                return false;
            }
        }
    }

    public static function getDir(){
        $path = Yii::$app->params['frontUrl'] . 'uploads';
        return $path;
    }
}