<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rubrics".
 *
 * @property integer $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_uk
 * @property integer $status
 */
class Rubrics extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rubrics';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_ru', 'name_en', 'name_uk'], 'required'],
            [['status'], 'integer'],
            [['name_ru', 'name_en', 'name_uk'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_ru' => 'Name Ru',
            'name_en' => 'Name En',
            'name_uk' => 'Name Uk',
            'status' => 'Status',
        ];
    }
   public static  function getAllRubrics() {
     $rubrics =  self::findAll(['status' => '1']);
       $nameArr = array();
       $lang = Yii::$app->params['deflang'];
       $name ='name_'.$lang;
       foreach($rubrics as $r) {
           $nameArr[$r->id]=$r->$name;
       }
       return $nameArr;
    }
    public static function getAllRubricsNews($rubrics){
        $rubIds = array();
        foreach($rubrics as $r) {
            $rubIds[]=$r->id;
        }
        return $rubIds;
    }

    public function getNews(){
        return $this->hasMany(News::className(), ['id' => 'new_id'])
            ->where(['status'=>News::STATUS_ACTIVE])
            ->orderBy([
                'created_at' => SORT_DESC
            ])
            ->viaTable('rubrics_news', ['rubric_id' => 'id'])
             ->viaTable('tags_news', ['new_id' => 'id']);

    }

    static function getRubricsNews($id) {
        return self::find()
            ->where(['id' => $id])
            ->with('news')
            ->asArray()
            ->all();
    }
}
