<?php
namespace common\models;

use Yii;
use yii\base\ErrorException;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property string $steamId
 * @property integer $discount
 * @property integer $balance
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    const STATUS_BANNED = 0;
    const STATUS_NOT_BANNED = 1;

    const STEAM_API_KEY = '8F526B6DD610D0EEE55C4E3DE6846559';

    /**
     * @var array EAuth attributes
     */
    public $profile;
    public $authKey;
    static $users;

    public static function getStatus($status = false){
        $statuses = [
            self::STATUS_DELETED => 'Продажи запрещены',
            self::STATUS_ACTIVE => 'Активен',
        ];
        return (false === $status) ? $statuses : isset($statuses[$status]) ? $statuses[$status] : $status;
    }
    public static function getStatusChat($status){
        return ($status) ? 'Активен' : 'Бан';
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['discount', 'default', 'value' => 0],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'username'   => 'Имя пользователя',
            'email'      => 'E-mail',
            'status'     => 'Статус',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
            'steamID'    => 'Steam ID',
            'chat'       => 'Статус пользователя в чате',
            'balance'    => 'Баланс',
            'discount'    => 'Скидка',
        ];
    }

    /**
     * @inheritdoc
     */
//    public static function findIdentity($id)
//    {
//        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
//    }

    public static function findIdentity($id) {
//        var_dump($id); die;
        if (Yii::$app->getSession()->has('user-'.$id)) {
            return new self(Yii::$app->getSession()->get('user-'.$id));
        }
        else {
            return isset(self::$users[$id]) ? new self(self::$users[$id]) : null;
        }
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public static function findByEAuth($service) {
        if (!$service->getIsAuthenticated()) {
            throw new ErrorException('EAuth user should be authenticated before creating identity.');
        }
        $id = $service->getServiceName().'-'.$service->getId();
        $name = $service->getAttribute('name');
        $attributes = array(
            'id' => $id,
            'username' => $name,
            'authKey' => md5($id),
            'profile' => $service->getAttributes(),
        );
//        var_dump($userSave);die();
        $attributes['profile']['service'] = $service->getServiceName();
        Yii::$app->getSession()->set('user-'.$id, $attributes);
        return new self($attributes);
    }

    public static function getSteamName($id){
        $html = file_get_contents('http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=' . self::STEAM_API_KEY . '&steamids=' . $id);
        $html = json_decode($html);
        if(isset($html->response->players[0]->personaname)){
            $html = $html->response->players[0]->personaname;
        } else {
            $html = file_get_contents('http://steamcommunity.com/profiles/' . $id);
            $html = explode('"actual_persona_name">', $html);

            $html = $html[1];
            $html = explode('</span>', $html);
            $html = $html[0];
        }

        $userSave = User::saveUserSteam($id, $html);
        return $html;
    }

    public static function getAvatar($id){
        $html = file_get_contents('http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=' . self::STEAM_API_KEY . '&steamids=' . $id);
        $html = json_decode($html);

        if(isset($html->response->players[0]->avatar)){
            $html = $html->response->players[0]->avatar;
        } else {
            $html = file_get_contents('http://steamcommunity.com/profiles/' . $id);
            $html = explode('"playerAvatarAutoSizeInner">', $html);

            $html = $html[1];
            $html = explode('"', $html);
            $html = $html[1];
        }
        return $html;
    }

    public static function saveUserSteam($id, $name){

        $userSteam = self::find()->where(['steamId'=>$id])->one();

        $slf = "http://steamcommunity.com/profiles/".$id."/?xml=1";
        $slf = simplexml_load_file($slf);

        if($userSteam==NULL) {
            $userSteam = new User;
            $userSteam->steamId = $id;
            $userSteam->created_at = time();
            $userSteam->updated_at = time();
            $userSteam->status = self::STATUS_ACTIVE;
            $userSteam->username = $name;
            $userSteam->email = $id;

            if ($userSteam->save()) {

            }

        }
        return $userSteam;

    }

    public static function getCurrentUser(){
        $identity = Yii::$app->getUser()->getIdentity();

        if(!is_null($identity) && isset($identity->profile['name'])){
            $id = $identity->profile['name'];
            $user = User::find()->where(['steamId' => $id])->one();
            return $user;
        }
        return false;
    }
    public static function getUserName($id) {
        $user = self::find()->where(['id'=>$id])->one();
        if (isset($user)) {
            return $user->username;
        }
         else return '';

    }
    public static function getUserIdBySteam($id) {
        $user = self::find()->where(['steamId'=>$id])->one();
        if (isset($user)) {
            return $user->id;
        }
        else return 1;

    }
    static function getUserDiscount($id){
        $user = self::find()
            ->where(['id' => $id ])
//            ->andWhere(['status'=>Trades::STATUS_COMPLETE])
            ->one();
        return $user->discount;
    }
    static function getBuyerTrades($id){
        return Trades::find()
            ->where(['buyer_id' => $id ])
            ->andWhere(['status'=>Trades::STATUS_COMPLETE])
            ->all();
    }

    static function getDiscountByTrades($trades,$discounts) {
        $amount = 0;
        $params = array();
        foreach($trades as $tr){
            $amount = $amount + (int)$tr->amount;
        }
        $params['amount'] = $amount;
        foreach($discounts as $elem) {
            if ($amount>=$elem->amount) {
                $params['discount'] = $elem->discount;
                return $params;
            }
        }
    }

    public function checkEmail(){
         return filter_var($this->email, FILTER_VALIDATE_EMAIL);
    }

    public static function getAllUsers(){
        $list = [];
        $users = self::find()->all();
        foreach ($users as $user) {
            $list[$user->id] = $user->username;
        }

        return $list;
    }
}


