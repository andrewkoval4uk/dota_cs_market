<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "faqs".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Faqs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faqs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Состояние вопроса',
            'created_at' => 'Создано',
            'updated_at' => 'Обновленно',
        ];
    }

    public static function getQuestion($id) {
        $faq = FaqContent::find()->where(['faq_id' => $id, 'lang' => Yii::$app->params['lang']])->one();
        return $faq->question;
    }
    public static function getAnswer($id) {
        $faq = FaqContent::find()->where(['faq_id' => $id, 'lang' => Yii::$app->params['lang']])->one();
        return $faq->answer;
    }
    public function getFaqContent(){
        return $this->hasMany(FaqContent::className(), ['faq_id' => 'id']);
    }

}
