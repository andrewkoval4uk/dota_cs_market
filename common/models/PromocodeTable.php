<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "promocode_table".
 *
 * @property integer $id
 * @property integer $value
 * @property string $name
 * @property integer $used_by
 * @property integer $used_at
 * @property integer $status
 * @property integer $created_at
 */
class PromocodeTable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promocode_table';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value', 'name'], 'required'],
            [['value', 'used_by', 'used_at', 'status', 'created_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Скидка(%)',
            'name' => 'Промокод',
            'used_by' => 'Использовать с',
            'used_at' => 'Использовать до',
            'status' => 'Статус',
            'created_at' => 'Создано',
        ];
    }

    public static function getPromoCode(){
        $chars = '12345ABCDEFGHIJKLMNOPQRSTUVWXYZ67890';
        $hashpromo = '';
        for($ichars = 0; $ichars < 9; ++$ichars) {
            $random = str_shuffle($chars);
            $hashpromo .= $random[0];
        }
        return $hashpromo;
    }
    static function getStatus() {
        return $statuses = array(
            0 => 'Новый',
            1 => 'Используется',
        );
    }
}
